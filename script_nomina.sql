CREATE TABLE sucursal(
	id serial,
	nombre varchar(80) not null,
	direccion varchar,
	telefono varchar(18),
	estado varchar(10) CHECK (estado IN ('activo','inactivo')) default 'activo',
	constraint pk_sucursal primary key (id)
);
CREATE TABLE departamento(
	id serial,
	nombre varchar(80) not null,
	estado varchar(10) CHECK (estado IN ('activo','inactivo')) default 'activo',
	constraint pk_departamento primary key (id)
);
CREATE TABLE sucursal_departamento(
	sucursal int,
	departamento int,
	constraint pk_sucursal_departamento primary key (sucursal,departamento),
	constraint fk_sucursal_sucursal_departamento foreign key (sucursal) references sucursal(id),
	constraint fk_departamento_sucursal_departamento foreign key (departamento) references departamento(id)
);
CREATE TABLE cargo(
	id serial,
	nombre varchar(80) not null,
	descripcion varchar (200),
	estado varchar(10) CHECK (estado IN ('activo','inactivo')) default 'activo',
	constraint pk_cargo primary key (id)
);
CREATE TABLE empleado(
	letra char CHECK (letra IN ('V','E')),
	cedula varchar(12) not null,
	nombre varchar(40) not null,
	apellido varchar(40) not null,
	direccion varchar(100),
	telefono varchar(18),
	discapacidad varchar(100),
	sexo char CHECK (sexo IN ('m','f')),
	fec_nac date,
	email varchar(60),
	cargo int,
	departamento int,
	sucursal int,
	sueldo numeric(16,2),
	estado varchar(10) CHECK (estado IN ('activo','inactivo')) default 'activo',
	fecha_ingreso date,
	constraint pk_empleado primary key (cedula),
	constraint pk_empleado_cargo foreign key (cargo) references cargo (id),
	constraint pk_empleado_departamento foreign key (departamento) references departamento (id),
	constraint pk_empleado_sucursal foreign key (sucursal) references sucursal (id)
);
CREATE TABLE historico(
	id serial,
	cedula varchar(12) not null,
	fecha_inicio date,
	fecha_final date,
	cargo int,
	sueldo numeric(16,2),
	constraint pk_historico primary key (id),
	constraint fk_historico_empleado foreign key (cedula) references empleado (cedula),
	constraint fk_historico_cargo foreign key (cargo) references cargo (id)
);
CREATE TABLE asignacion(
	id serial, 
	nombre varchar(40) not null,
	monto numeric(16,2),
	porcentaje numeric(16,2),
	estado char,
	constraint pk_asignacion primary key (id)
);
CREATE TABLE deduccion(
	id serial, 
	nombre varchar(40) not null,
	monto numeric(16,2),
	porcentaje numeric(16,2),
	estado char,
	constraint pk_deducion primary key (id)
);
CREATE TABLE asignacion_empleado(
	id serial, 
	cedula varchar(12) not null,
	asignacion int not null,
	monto numeric(16,2),
	fecha date default current_date,
	tipo varchar(30),
	constraint pk_asignacion_empleado primary key (id),
	constraint fk_empleado_asignacion_empleado foreign key (cedula) references empleado (cedula),
	constraint fk_asignacion_asignacion_empleado foreign key (asignacion) references asignacion (id)
);
CREATE TABLE deduccion_empleado(
	id serial, 
	cedula varchar(12) not null,
	deduccion int not null,
	monto numeric(16,2),
	fecha date default current_date,
	tipo varchar(30),
	constraint pk_deduccion_empleado primary key (id),
	constraint fk_empleado_deduccion_empleado foreign key (cedula) references empleado (cedula),
	constraint fk_deduccion_deduccion_empleado foreign key (deduccion) references deduccion (id)
);
CREATE TABLE horario(
	id serial, 
	hora_entrada time,
	hora_salida time,
	jornada int,
	constraint pk_horario primary key (id)
);
CREATE TABLE asistencia(
	cedula varchar(12),
	fecha date default current_date,
	hora_entrada time default current_time,
	hora_salida time,
	constraint pk_asistencia primary key (cedula,fecha),
	constraint fk_empleado_asistencia foreign key (cedula) references empleado (cedula)
);
CREATE TABLE hora_extra(
	cedula varchar(12) not null,
	fecha date,
	cantidad int,
	monto numeric(16,2),
	constraint pk_hora_extra primary key (cedula,fecha),
	constraint fk_empleado_hora_extra foreign key (cedula) references empleado (cedula)
);

CREATE TABLE dias_feriados(
	id serial,
	cedula varchar(12) not null,
	fecha date,
	cant_dias int,
	monto numeric(16,2),
	constraint pk_dias_feriados primary key (id),
	constraint fk_empleado_dias_feriados foreign key (cedula) references empleado (cedula)
);
CREATE TABLE nomina(
	fecha date,
	mes int,
	anio int,
	cedula varchar(12),
	sueldo_diario numeric(16,2),
	total_asignaciones numeric(16,2),
	total_deducciones numeric(16,2),
	horas_extras numeric(16,2),
	dias_feriados numeric(16,2),
	sueldo_neto numeric(16,2),
	cestaticket numeric(16,2),
	total_hExtras integer,
	constraint pk_nomina primary key (mes,anio,cedula),
	constraint fk_empleado_nomina foreign key (cedula) references empleado (cedula)
);

CREATE TABLE feriados(
	fecha date,
	motivo varchar(50)
);

CREATE TABLE configuracion(
	sueldo_minimo numeric(16,2) default 0.0,
	cestaticket numeric(16,2) default 0.0,
	unidad_tributaria numeric(16,2)
);

CREATE TABLE usuario(
	cedula varchar (15),
	nombre varchar(15),
	clave varchar(15),
	tipo varchar(20),
	sucursal int,
	constraint fk_sucursal_usuario foreign key (sucursal) references sucursal (id)
);

INSERT INTO usuario values('22650767','admin','1234','superadmin');
INSERT INTO usuario values('21322117','admin','1234','superadmin');
INSERT INTO usuario values('20536755','admin','1234','admin');
