'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
    .controller('EmpleadoCtrl', function($scope, webService, $mdToast, $mdDialog, DTOptionsBuilder) {

        $scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');

        $scope.new_empleado = { cedula: "", nombre: "", apellido: "", direccion: "", telefono: "", discapacidad: "", sexo: "", fec_nac: "", email: "", cargo: "", sueldo: "" };
        $scope.estado = {};
        $scope.estado.estado = 'activo';

        $scope.myDate = new Date();
        $scope.maxDate = new Date(
            $scope.myDate.getFullYear() - 16,
            $scope.myDate.getMonth(),
            $scope.myDate.getDate());

        $scope.maxDateIngreso = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth(),
            $scope.myDate.getDate());

        $scope.new_empleado.fecha_ingreso = $scope.myDate;


        $scope.cargarEmpleados = function(estado) {
            webService.empleados(estado).promise.then(function(data) {
                $scope.empleados = data.data;
                $scope.showSimpleToast(data.msg);

            })
        }

        $scope.cargarEmpleados($scope.estado);


        $scope.editar = function(ev, empleado) {
            loadSucursal();
            $scope.load_departamentos(empleado.sucursal);
            $scope.empleadoEdit = angular.copy(empleado);
            $scope.empleadoEdit.fec_nac = new Date(empleado.fec_nac);
            $mdDialog.show({
                controller: DialogController,
                scope: $scope.$new(),
                templateUrl: 'modules/empleado/editarEmpleado.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        };

        $scope.info = function(ev, empleado) {
            $scope.infoEm = angular.copy(empleado);
            $mdDialog.show({
                controller: DialogController,
                scope: $scope.$new(),
                templateUrl: 'modules/empleado/infoEmpleado.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        };

        $scope.nuevoEmpleado = function(ev) {
            loadCargos();
            loadSucursal();
            $mdDialog.show({
                controller: DialogController,
                scope: $scope.$new(),
                templateUrl: 'modules/empleado/nuevoEmpleado.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        };


        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }

        $scope.newEmpleado = function() {
            var newEmpleado = angular.copy($scope.new_empleado);
            newEmpleado.fec_nac = moment(newEmpleado.fec_nac).format("YYYY-MM-DD");
            newEmpleado.fecha_ingreso = moment(newEmpleado.fecha_ingreso).format("YYYY-MM-DD");
            webService.newEmpleados(newEmpleado).promise.then(function(data) {
                if (data.success) {
                    $mdDialog.cancel();
                    $scope.showSimpleToast(data.msg); //mensaje de guardado
                    $scope.estado.estado = 'activo';
                    $scope.cargarEmpleados($scope.estado);
                    $scope.new_empleado = {};
                    $scope.new_empleado.fecha_ingreso = $scope.myDate;

                } else {
                    $scope.showSimpleToast(data.msg); //mensaje de guardado
                }
            })
        }

        $scope.SaveUpdate = function(empleadoEdit) {
            delete empleadoEdit.nombre_cargo;
            empleadoEdit.fec_nac = moment(empleadoEdit.fec_nac).format("YYYY-MM-DD");

            webService.editEmpleados(empleadoEdit).promise.then(function(data) {
                if (data.success) {
                    $mdDialog.cancel();
                    $scope.estado.estado = 'activo';
                    $scope.cargarEmpleados($scope.estado);
                    $scope.showSimpleToast(data.msg); //mensaje de guardado
                }
            })
        }



        function loadCargos() {
            var estado = {};
            estado.estado = 'activo';

            webService.cargos(estado).promise.then(function(data) {
                if (data.success) {
                    $scope.cargos = data.data;
                }
            })
        }

        function loadSucursal() {
            var estado = {};
            estado.estado = 'activo';
            webService.sucursales(estado).promise.then(function(data) {
                $scope.sucursal = data.data;
            })
        }

        $scope.load_departamentos = function(sucursal) {
            for (var i = 0; i < $scope.sucursal.length; i++) {
                if (sucursal == $scope.sucursal[i].id) {
                    $scope.departamentos = $scope.sucursal[i].departamentos;
                }
            }
            console.log($scope.departamentos);
        }

        $scope.showSimpleToast = function(text) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(text)
                .hideDelay(3000)
                .position('top right')
            );
        };


    });
