'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('CargoCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.new_cargo={nombre:"",descripcion:""};
    $scope.estado = {};
    $scope.estado.estado='activo';

    $scope.cargarCargos = function(estado){
    	webService.cargos(estado).promise.then(function(data){
    		$scope.cargos = data.data;
            $scope.showSimpleToast(data.msg);
    	})
    }

    $scope.cargarCargos($scope.estado);


	$scope.editar = function(ev,cargo) {
        $scope.cargoEdit=angular.copy(cargo);
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/cargo/editarCargo.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   });
	 };

	$scope.nuevoCargo = function(ev) {
	   $mdDialog.show({
	     controller: DialogController,
	     scope:$scope.$new(),
	     templateUrl: 'modules/cargo/nuevoCargo.html',
	     parent: angular.element(document.body),
	     targetEvent: ev,
	     clickOutsideToClose:true,
	   });
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.newCargo = function(){
    	webService.newCargo($scope.new_cargo).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
    			$scope.estado.estado='activo';
				$scope.cargarCargos($scope.estado);
                $scope.new_cargo={nombre:"",descripcion:""};
    		}
    	})
    }
 	
 	$scope.SaveUpdate = function(cargoEdit){
    	webService.editCargo(cargoEdit).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
    			$scope.estado.estado='activo';
				$scope.cargarCargos($scope.estado);
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
				
    		}
    	})
    }
 	
 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};


 });
