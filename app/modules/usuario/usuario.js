angular.module('materialAngularApp') 
	.controller('UsuarioCtrl', function($scope, webService, $mdToast, $mdDialog, DTOptionsBuilder) {

        $scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');

        function usuarios() {
            webService.usuarios().promise.then(function(data) {
                if (data.success) {
                    $scope.usuarios = data.data;
                }
            });
        }
        usuarios();

        function loadSucursal() {
            var estado = {};
            estado.estado = 'activo';
            webService.sucursales(estado).promise.then(function(data) {
                $scope.sucursal = data.data;
            })
        }
        $scope.new_usuario = {};
        $scope.nuevoUsuario = function(ev) {
            loadSucursal();
            $scope.modEditar=false;
            $mdDialog.show({
                controller: DialogController,
                scope: $scope.$new(),
                templateUrl: 'modules/usuario/nuevoUsuario.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        };


        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        $scope.formUsuario = function(){
            if (!$scope.modEditar) {
                $scope.newUsuario();
            }else{
                $scope.editUsuario();
            }
        }
        $scope.newUsuario = function() {
            webService.newUsuario($scope.new_usuario).promise.then(function(data) {
                if (data.success) {
                    $mdDialog.cancel();
                    $scope.showSimpleToast(data.msg); //mensaje de guardado
                    usuarios();
                    $scope.new_usuario = {};
                } else {
                    $scope.showSimpleToast(data.msg); //mensaje de error
                }
            })
        } 
        $scope.editUsuario = function() {
            webService.editUsuario($scope.new_usuario).promise.then(function(data) {
                if (data.success) {
                    $mdDialog.cancel();
                    $scope.showSimpleToast(data.msg); //mensaje de guardado
                    usuarios();
                    $scope.new_usuario = {};
                } else {
                    $scope.showSimpleToast(data.msg); //mensaje de error
                }
            })
        }

        $scope.editar = function(ev, usuario) {
            loadSucursal();
            $scope.new_usuario =  usuario;
            $scope.modEditar=true;
            $mdDialog.show({
                controller: DialogController,
                scope: $scope.$new(),
                templateUrl: 'modules/usuario/nuevoUsuario.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        };
         $scope.showSimpleToast = function(text) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(text)
                .hideDelay(3000)
                .position('top right')
            );
        };


});
