'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('AsignacionCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.new_asignacion={nombre:"",monto:"", porcentaje:"", estado:"f"};
    $scope.estado = {};
    $scope.estado.estado='asignacion';

    $scope.cargarAsignaciones = function(estado){
    	if (estado.estado=='asignacion') {
	    	webService.asignaciones().promise.then(function(data){
	    		$scope.asign_deduccion = data.data;
	            //$scope.showSimpleToast(data.msg);
	    	})
    	}else{
			webService.deducciones().promise.then(function(data){
				$scope.asign_deduccion = data.data;
		        //$scope.showSimpleToast(data.msg);
			})
    	}
    }

    $scope.cargarAsignaciones($scope.estado);


	$scope.editar = function(ev,asig_deducc) {
	    if ($scope.estado.estado=='asignacion') {
			$scope.title='Editar Asignación'
	    }else{
			$scope.title='Editar Deducción'
	    }
        $scope.dataEdit=angular.copy(asig_deducc);
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/asignacion-deduccion/editar.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   });
	 };

	$scope.nuevaAsignacion = function(ev,type) {
		$scope.type=type;
		if (type=='asignacion') {
			$scope.title='Nueva asignacion'
		}else{
			$scope.title='Nueva deducción'
		}
	   $mdDialog.show({
	     controller: DialogController,
	     scope:$scope.$new(),
	     templateUrl: 'modules/asignacion-deduccion/nueva.html',
	     parent: angular.element(document.body),
	     targetEvent: ev,
	     clickOutsideToClose:true,
	   });
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.newAsig_Deduc = function(){

    	if ($scope.type=='asignacion') {
	    	webService.newAsignacion($scope.new_asignacion).promise.then(function(data){
	    		if (data.success) {
	            	$mdDialog.cancel();
	            	$scope.showSimpleToast(data.msg);//mensaje de guardado
	    			$scope.estado.estado=$scope.type;
					$scope.cargarAsignaciones($scope.estado);
    				$scope.new_asignacion={nombre:"",monto:"", porcentaje:"", estado:"f"};
	    		}
	    	});
    	}else{
	    	webService.newDeduccion($scope.new_asignacion).promise.then(function(data){
	    		if (data.success) {
	            	$mdDialog.cancel();
	            	$scope.showSimpleToast(data.msg);//mensaje de guardado
	    			$scope.estado.estado=$scope.type;
					$scope.cargarAsignaciones($scope.estado);
    				$scope.new_asignacion={nombre:"",monto:"", porcentaje:""};
	    		}
	    	});
    	}
    }
 	
 	$scope.SaveUpdate = function(dataEdit){
	    if ($scope.estado.estado=='asignacion') {
	    	webService.editAsignacion(dataEdit).promise.then(function(data){
	    		if (data.success) {
	            	$mdDialog.cancel();
	            	$scope.showSimpleToast(data.msg);//mensaje de guardado
					$scope.cargarAsignaciones($scope.estado);
	    		}
	    	})
	    }else{
	    	webService.editDeduccion(dataEdit).promise.then(function(data){
	    		if (data.success) {
	            	$mdDialog.cancel();
	            	$scope.showSimpleToast(data.msg);//mensaje de guardado
					$scope.cargarAsignaciones($scope.estado);
	    		}
	    	})
	    }
    }
 	
 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};


 });
