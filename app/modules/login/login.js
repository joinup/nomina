angular.module('materialAngularApp')
  .controller('LoginCtrl', function ($scope,$rootScope,$routeParams,webService,$mdDialog,$mdToast,$location){
  	console.log($location.url());
  	$rootScope.noBar=true;

  	$scope.login = function(user){
  		webService.login(user).promise.then(function(data){
  			$scope.showSimpleToast(data.msg);
  			if (data.success) {
  				$location.path("/");
  			};
  		})
  	}

  	$scope.showSimpleToast = function(text) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(text)
                .hideDelay(3000)
                .position('top right')
          );
    };

});