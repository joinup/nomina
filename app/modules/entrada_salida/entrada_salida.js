'use strict';

angular.module('materialAngularApp')
  	.controller('EntradaSalidaCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

	$scope.empleado={cedula:'',nombre:'',nombre_cargo:'',apellido:''};
	$scope.horaEntrada=true;
	$scope.horaSalida=true;

    $scope.loadEmpleado = function(){
        $scope.horaEntrada=true;
        $scope.horaSalida=true;
    	webService.empleado($scope.empleado.cedula).promise.then(function(data){
    		$scope.empleado = data.data[0];
            $scope.showSimpleToast(data.msg);
            asistencia();
    	})
        console.log($scope.horaEntrada);
        console.log($scope.horaSalida);
    }

    $scope.entradaSalida = function(option){
    	var day = moment().format("YYYY-MM-DD HH:mm:ss");
    	var hora = day.split(" ")[1];
    	var fecha = day.split(" ")[0];
    	var empleado={};
    	if (option=='entrada') {
    		empleado={cedula:$scope.empleado.cedula,fecha:fecha,hora_entrada:hora};
    	}else{
    		empleado={cedula:$scope.empleado.cedula,fecha:fecha,hora_salida:hora};
    	}

		webService.newAsistencia(empleado).promise.then(function(data){
			if (data.success) {
            	$scope.showSimpleToast(data.msg);
    			$scope.horaEntrada=true;
			}
            asistencia();
		})
         console.log($scope.horaEntrada);
        console.log($scope.horaSalida);
    		
    }

	// indica si un usuario debe introducir entrada o salida
    function asistencia(){
    	var day = moment().format("YYYY-MM-DD");
    	var empleado={cedula:$scope.empleado.cedula,fecha:day};
		webService.asistenciaEmpleado(empleado).promise.then(function(data){
			if (data.success) {
				if (data.data[0].hora_salida==null) {
					$scope.horaSalida=false;
    				$scope.horaEntrada=true;
    			}else{
    				$scope.horaEntrada=true;
    				$scope.horaSalida=true;
            		$scope.showSimpleToast("Ya su jornada de trabajo esta registrada");
    			}

			}else{
				$scope.horaEntrada=false;
                $scope.horaSalida=true;

			}
            console.log($scope.horaEntrada);
            console.log($scope.horaSalida);
		})
    }

 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};

 });
