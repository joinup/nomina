'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('AsistenciaCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.myDate = new Date();
    $scope.maxDate = new Date(
      $scope.myDate.getFullYear(),
      $scope.myDate.getMonth() ,
      $scope.myDate.getDate());

    var dataAsit ={};
    
    $scope.cargarAsistencia = function(dataAsit){
        dataAsit=moment(dataAsit).format("YYYY-MM-DD");
    	webService.asistencia({fecha:dataAsit}).promise.then(function(data){
    		$scope.asistencia = data.data;
            $scope.showSimpleToast(data.msg);
    	})
    }

    $scope.cargarAsistencia(dataAsit);


	$scope.editar = function(ev,sucursal) {
        $scope.sucursalEdit=angular.copy(sucursal);
        console.log($scope.sucursalEdit);
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/editarSucursal.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   });
	 };

	$scope.nuevaSucursal = function(ev) {
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/nuevaSucursal.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.newSucursal = function(){
    	webService.newSucursal($scope.new_sucursal).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
    			$scope.estado.estado='activo';
				$scope.cargarSucursales($scope.estado);
    		}
    	})
    }
 	
 	$scope.SaveUpdate = function(sucursalEdit){
    	webService.editSucursal(sucursalEdit).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
    			$scope.estado.estado='activo';
				$scope.cargarSucursales($scope.estado);
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
				
    		}
    	})
    }


    $scope.AgregarDepa = function(ev,departamento){
    	$scope.update_Dep=departamento;
    	departamentos();
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/nuevoDepartamento.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
    }
 	
    function departamentos(){

    	webService.departamentos().promise.then(function(data){
    		if (data.success) {
				$scope.departamentos=data.data;
    		}
    	});
    }

    $scope.updateDep = function (){
    	webService.departamentos().promise.then(function(data){
    		if (data.success) {
				$scope.departamentos=data.data;
    		}
    	});
    }

 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};

 });
