'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('DepartamentoCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.new_departamento={nombre:""};
    $scope.estado = {};
    $scope.estado.estado='activo';

    $scope.cargarDepartamentos = function(estado){
    	webService.departamentos(estado).promise.then(function(data){
    		$scope.departamentos = data.data;
            $scope.showSimpleToast(data.msg);
    	})
    }

    $scope.cargarDepartamentos($scope.estado);


	$scope.editar = function(ev,departamento) {
		sucursales();
        $scope.departamentoEdit=angular.copy(departamento);
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/departamento/editarDepartamento.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   });
	 };

	$scope.nuevoDepartamento = function(ev) {
		sucursales();
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/departamento/nuevoDepartamento.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.newDepartamento = function(){
    	webService.newDepartamentos($scope.new_departamento).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
    			$scope.estado.estado='activo';
				$scope.cargarDepartamentos($scope.estado);
    			$scope.new_departamento={nombre:""};

    		}
    	})
    }
 	
 	$scope.SaveUpdate = function(departamentoEdit){
 		delete departamentoEdit.sucursales;
    	webService.editDepartamentos(departamentoEdit).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
    			$scope.estado.estado='activo';
				$scope.cargarDepartamentos($scope.estado);
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
				
    		}
    	})
    }

    function sucursales(){
    	var estado={};
    	estado.estado='activo';

    	webService.sucursales(estado).promise.then(function(data){
    		if (data.success) {
				$scope.sucursales=data.data;
    		}
    	});
    }
 	
 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};

 });
