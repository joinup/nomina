'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # SucursalCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('SucursalCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.new_sucursal={nombre:"",direccion:"", telefono:""};
    $scope.estado = {};
    $scope.estado.estado='activo';

    $scope.cargarSucursales = function(estado){
    	webService.sucursales(estado).promise.then(function(data){
    		$scope.sucursal = data.data;
            $scope.showSimpleToast(data.msg);
    	})
    }

    $scope.cargarSucursales($scope.estado);


	$scope.editar = function(ev,sucursal) {
        $scope.sucursalEdit=angular.copy(sucursal);
        console.log($scope.sucursalEdit);
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/editarSucursal.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   });
	 };

	$scope.nuevaSucursal = function(ev) {
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/nuevaSucursal.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.newSucursal = function(){
    	webService.newSucursal($scope.new_sucursal).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
    			$scope.estado.estado='activo';
				$scope.cargarSucursales($scope.estado);
    		}
    	})
    }
 	
 	$scope.SaveUpdate = function(sucursalEdit){
        delete sucursalEdit.departamentos;
    	webService.editSucursal(sucursalEdit).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
    			$scope.estado.estado='activo';
				$scope.cargarSucursales($scope.estado);
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
				
    		}
    	})
    }


    $scope.AgregarDepa = function(ev,departamento){
    	$scope.update_Dep=departamento;
    	departamentos();
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/sucursal/nuevoDepartamento.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
    }
 	
    function departamentos(){

    	webService.departamentos().promise.then(function(data){
    		if (data.success) {
				$scope.departamentos=data.data;
    		}
            for (var i = 0; i < $scope.update_Dep.departamentos.length; i++) {
                for (var j = 0; j < $scope.departamentos.length; j++) {
                    if ($scope.update_Dep.departamentos[i].id==$scope.departamentos[j].id) {
                        $scope.departamentos[j].selected=true;
                    }
                }
            }
        });
    }

    $scope.updateDep = function (departamentos){
        var suc_depa={};
        suc_depa.sucursal=departamentos.id;
        suc_depa.departamento=departamentos.departamento;
    	webService.asignarDepartamentoSucursal(suc_depa).promise.then(function(data){
    		$scope.showSimpleToast(data.msg);
            $mdDialog.cancel();
            $scope.estado.estado='activo';
            $scope.cargarSucursales($scope.estado);
    	});
    }

 	$scope.showSimpleToast = function(text) {
 	    $mdToast.show(
 	        $mdToast.simple()
 	            .textContent(text)
 	            .hideDelay(3000)
 	            .position('top right')
 	      );
 	};

 });
