'use strict';

/**
 * @ngdoc overview
 * @name materialAngularApp
 * @description
 * # materialAngularApp
 *
 * Main module of the application.
 */
angular
  .module('materialAngularApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMaterial',
    /*'ngMenuSidenav',*/
    'datatables',
    'md.data.table'

  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'modules/home/home.html',
        controller: 'HomeCtrl'
      })
      .when('/sucursal', {
        templateUrl: 'modules/sucursal/sucursal.html',
        controller: 'SucursalCtrl'
      })
      .when('/departamento', {
        templateUrl: 'modules/departamento/departamento.html',
        controller: 'DepartamentoCtrl'
      })
      .when('/empleado', {
        templateUrl: 'modules/empleado/empleado.html',
        controller: 'EmpleadoCtrl'
      })
      .when('/cargo', {
        templateUrl: 'modules/cargo/cargo.html',
        controller: 'CargoCtrl'
      })
      .when('/asignacion-deduccion', {
        templateUrl: 'modules/asignacion-deduccion/asignacion-deduccion.html',
        controller: 'AsignacionCtrl'
      })
      .when('/asistencia', {
        templateUrl: 'modules/asistencia/asistencia.html',
        controller: 'AsistenciaCtrl'
      })
      .when('/configuracion', {
        templateUrl: 'modules/configuracion/configuracion.html',
        controller: 'ConfiguracionCtrl'
      })
      .when('/login', {
        templateUrl: 'modules/login/login.html',
        controller: 'LoginCtrl'
      })
      .when('/nomina', {
        templateUrl: 'modules/nomina/nomina.html',
        controller: 'NominaCtrl'
      })
      .when('/entrada_salida', {
        templateUrl: 'modules/entrada_salida/entrada_salida.html',
        controller: 'EntradaSalidaCtrl'
      })
      .when('/usuarios', {
        templateUrl: 'modules/usuario/usuario.html',
        controller: 'UsuarioCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })

.directive('header', function () {
      return {
        restrict: 'A',
        replace: true,
        templateUrl:'modules/header.html'
      }
  })
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('green',{'default': '700'});
    })
  .run(function ($rootScope, webService,$location) {
    $rootScope.logged=true;
    $rootScope.cargando=true;
  })
  .run(function ($rootScope, webService,$location) {
    $rootScope.cargando=true;
    $rootScope.logged=true;
    webService.checkSession().promise.then(function(data){
        console.log(data)
        if (data.success) {
            $rootScope.currentUser=data.data;
            $rootScope.logged=true;
            if($location.url()=="/login")
                $location.path("/")
        }else{
            $rootScope.logged=false;
            $location.path("/login")
        };
    });
    $rootScope.$on('logout',function(){
            $rootScope.currentUser=null; 
            $rootScope.logged=false;
            $location.url("/login");
            webService.logout().promise.then(function(){});
        });
    $rootScope.logout = function(){
        $rootScope.currentUser=null; 
        $rootScope.logged=false;
        $location.url("/login");
        webService.logout().promise.then(function(){});
    }
  })

  .run(function ($rootScope, webService,$location,$mdSidenav) {
    $rootScope.$on('$routeChangeStart', function (event, next,current) {
        $rootScope.cargando=true;
        $rootScope.logged=true;
        $mdSidenav('left').close();
        webService.checkSession().promise.then(function(data){
            if (data.success) {
                $rootScope.currentUser=data.data;
                $rootScope.logged=true;
                if($location.url()=="/login")
                    $location.path("/")
            }else{
                $rootScope.logged=false;
                $location.path("/login")
            };
        });
    });
  })
  /*.run(function(amMoment) {
    amMoment.changeLocale('es');
  })*/

.controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.index = 0;

    $scope.toggleSidenav = function (menuId) {
        $mdSidenav(menuId)
        //.isOpen();
        .toggle();
    };
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });
    };
});