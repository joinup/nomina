'use strict';

/**
 * @ngdoc function
 * @name materialAngularApp.controller:SucursalCtrl
 * @description
 * # ConfiguracionCtrl
 * Controller of the materialAngularApp
 */
angular.module('materialAngularApp')
  	.controller('ConfiguracionCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;
    $scope.editUt=true;
    $scope.editSueldo=true;
    $scope.editEntrada=true;
    $scope.feriado={};
    $scope.feriado.fecha = new Date();

    $scope.cargarConfiguración = function(){
        webService.configuracion().promise.then(function(data){
            $scope.configuracion = data.data[0];
        })
    }
    
    function cargarHorario(){
    	webService.horario().promise.then(function(data){
            if (data.success) {
        		$scope.horario = data.data[0];
                var entrada=$scope.horario.hora_entrada.split(":");
                var salida=$scope.horario.hora_salida.split(":");
                $scope.horario.hora_entrada=new Date(1970, 0, 1, entrada[0], entrada[1], entrada[2]);
                $scope.horario.hora_salida=new Date(1970, 0, 1, salida[0], salida[1], salida[2]);
            }
    	})
    }

    $scope.cargarConfiguración();
    diasFeriados();
    cargarHorario();

	$scope.editar = function(option) {
        if (option=='sueldo') {
            $scope.editSueldo=!$scope.editSueldo;
        }else{
            $scope.editUt=!$scope.editUt;
        }
	 };

     $scope.editarHorario = function(){
        $scope.editEntrada=!$scope.editEntrada;
     }

     $scope.saveUpdateHorario = function(){
        $scope.horario.hora_entrada=moment($scope.horario.hora_entrada).format('HH:mm:ss');
        $scope.horario.hora_salida=moment($scope.horario.hora_salida).format('HH:mm:ss');
        webService.editHorario($scope.horario).promise.then(function(data){
            if (data.success) {
                cargarHorario();
                $scope.showSimpleToast(data.msg);//mensaje de guardado
                $scope.editEntrada=!$scope.editEntrada;
            }
        });
     }

	$scope.nuevoFeriado = function(ev) {
	   	$mdDialog.show({
			controller: DialogController,
			scope:$scope.$new(),
			templateUrl: 'modules/configuracion/feriado.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
	   	});
	 };

	function DialogController($scope, $mdDialog){
	    $scope.hide = function() {
	      $mdDialog.hide();
	    };

	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };

	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	}

    $scope.SaveFeriado = function(feriado){
        feriado.fecha=moment(feriado.fecha).format("YYYY-MM-DD");
    	webService.newFeriado(feriado).promise.then(function(data){
    		if (data.success) {
            	$mdDialog.cancel();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
                diasFeriados()
    		}
    	})
    }
 	
 	$scope.saveUpdate = function(option){
    	webService.editConfig($scope.configuracion).promise.then(function(data){
    		if (data.success) {
				$scope.cargarConfiguración();
            	$scope.showSimpleToast(data.msg);//mensaje de guardado
				$scope.editar(option);
    		}
    	})
    }

    function diasFeriados(){
        webService.feriados().promise.then(function(data){
            if (data.success) {
                $scope.feriados=data.data;
            }
        });
    }

    $scope.showSimpleToast = function(text) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(text)
                .hideDelay(3000)
                .position('top right')
          );
    };


 });
