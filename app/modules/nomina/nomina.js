'use strict';

angular.module('materialAngularApp')
  	.controller('NominaCtrl', function ($scope,webService,$mdToast,$mdDialog,DTOptionsBuilder) {
    
  	$scope.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('espanol.json');;

    $scope.showCalculo=false;
    $scope.myDate = new Date();
    $scope.newAsigna={};
    $scope.agregadasAD={};
    $scope.showHoraExtra=false;


    var fecha1=moment();
    var fecha2=moment();

    $scope.minDate = new Date(
        $scope.myDate.getFullYear(),
        $scope.myDate.getMonth() - 2,
        $scope.myDate.getDate());

    $scope.maxDate = new Date(
        $scope.myDate.getFullYear(),
        $scope.myDate.getMonth() + 2,
        $scope.myDate.getDate());


    $scope.change = function(estado){
      $scope.showCalculo=estado;
    }

    $scope.cargarNomina = function(){
    	webService.nomina().promise.then(function(data){
    		$scope.nomina = data.data;
        $scope.showSimpleToast(data.msg);
    	})
    }

    $scope.cargarNomina();

    $scope.generarNomina = function() {
      empleadosEnNomina();
      $scope.showCalculo=true;
    };

    function empleadosEnNomina (){
      var asistencia={};
      fechaQuincena();

      asistencia.fecha1=fecha1;
      asistencia.fecha2=fecha2;
      webService.asistenciaNomina(asistencia).promise.then(function(data){
        $scope.empleados=data.data;
        if ($scope.nomina) {
          for (var i = 0; i < $scope.empleados.length; i++) {
            for (var j = 0; j < $scope.nomina.length; j++) {
              if (($scope.nomina[j].cedula==$scope.empleados[i].cedula) && $scope.nomina[j].fecha != null) {
                $scope.empleados[i].procesado=true;
              }
            }
          }
        }
      });
    }

    function fechaQuincena(){
      var dia=moment().format("DD");

      if (parseInt(dia)>=1 && parseInt(dia)<=15) {
        fecha1=moment().format("YYYY-MM-01");
        fecha2=moment().format("YYYY-MM-15");
      }else{
        fecha1=moment().format("YYYY-MM-16");
        fecha2=moment().format("YYYY-MM-31");
      }
    }

    $scope.nuevaAsignacion = function(ev,empleado,option) {
      $scope.asigDeduc={};
      $scope.asigDeduc.cedula=empleado.cedula;
      $scope.newAsigna.cedula=empleado.cedula;
      $scope.newAsigna.asignaciones=[];
      $scope.agregadasAD.asignaciones=[];
      
      if (option == 'asignacion') {
          $scope.title = 'Asignación';
          loadAsignacion();
          loadAsignacionEmpleado(empleado.cedula);
      }else{
          $scope.title = 'Deducción';
          loadDeduccion();
          loadDeduccionEmpleado(empleado.cedula);

      }
      $mdDialog.show({
      controller: DialogController,
      scope:$scope.$new(),
      templateUrl: 'modules/nomina/asignacion.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
     });
    };

    $scope.valor = function(asignacion){
      for (var i = 0; i < $scope.asigDeducciones.length; i++) {
        if($scope.asigDeducciones[i].id==asignacion){
          if ($scope.asigDeducciones[i].monto != null) {
            $scope.asigDeduc.monto=$scope.asigDeducciones[i].monto;
            $scope.asigDeduc.porcentaje=null;
          }else{
            $scope.asigDeduc.porcentaje=$scope.asigDeducciones[i].porcentaje;
            $scope.asigDeduc.monto=null;
          }
          $scope.asigDeduc.nombre=$scope.asigDeducciones[i].nombre;
        }
      }
    }

    $scope.addAsignacion = function(){
      if ($scope.asigDeduc) {
        $scope.newAsigna.asignaciones.push($scope.asigDeduc);
      }
      for (var i = 0; i < $scope.asigDeducciones.length; i++) {
        if ($scope.asigDeducciones[i].id==$scope.asigDeduc.asignacion) {
          $scope.asigDeducciones[i].disabled=true;
        }
      }
      $scope.asigDeduc={};
    }

    $scope.detalle = function(ev,empleado) {
      $scope.infoEmpleado=empleado;
      $mdDialog.show({
      controller: DialogController,
      scope:$scope.$new(),
      templateUrl: 'modules/nomina/infoNomina.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
     });
    };
    
    $scope.tiempoTrabajado = function(ev,empleado) {
      detalleTrabajado(empleado);
      $mdDialog.show({
      controller: DialogController,
      scope:$scope.$new(),
      templateUrl: 'modules/nomina/tiempoTrabajado.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
     });
    };

    function detalleTrabajado(cedula){
      fechaQuincena();
      var nomina={};
      $scope.totalHoraExtra={};
      $scope.totalHoraExtra.cedula=cedula.cedula;
      nomina.cedula=cedula.cedula;
      nomina.fecha1=fecha1;
      nomina.fecha2=fecha2;
      nomina.fecha=moment().format("YYYY-MM-DD");
      webService.tiempoTrabajado(nomina).promise.then(function(data){
        $scope.timeWorking=data.data;
      
        var asistencia=angular.copy($scope.timeWorking.asistencia);
        var horario=angular.copy($scope.timeWorking.horario);
        $scope.totalHoraExtra.hora='00:00:00';
        for (var i = 0; i < asistencia.length; i++) {
          if (horaMayor(horario.hora_salida,asistencia[i].hora_salida)==true) {
            var horaExtra=totalHoras(asistencia[i].hora_salida,horario.hora_salida,'2017-01-01');
            $scope.timeWorking.asistencia[i].hora_extra=horaExtra;
            $scope.totalHoraExtra.hora=sumHoras($scope.totalHoraExtra.hora,horaExtra,'2017-01-01');

          }else{
            delete $scope.timeWorking.asistencia[i];
          }
        }
        var working=[];
        for (var i = 0; i < $scope.timeWorking.asistencia.length; i++) {
          if (typeof($scope.timeWorking.asistencia[i])!= 'undefined') {
            working.push($scope.timeWorking.asistencia[i]);
          }
        }
        $scope.timeWorking.asistencia=working;
      });


    }

    //total de horas entre fecha y fecha
    function totalHoras(horaFinal, horaInicio, fecha){
      var hora_salida= moment(fecha+' '+horaFinal);
      console.log(hora_salida);
      var hora_entrada =  (horaInicio).split(':');
      var horasTrabajadas= hora_salida.subtract({hours:hora_entrada[0], minutes:hora_entrada[1], seconds:hora_entrada[2]});
      return moment(horasTrabajadas).format('HH:mm:ss');
    }

    function sumHoras(hora1,hora2,fecha){
      var hora1= moment(fecha+' '+hora1);
      var hora2 =  (hora2).split(':');
      var sumTotal= hora1.add({hours:hora2[0], minutes:hora2[1], seconds:hora2[2]});
      return moment(sumTotal).format('HH:mm:ss');
    }

    function horaMayor(hora1,hora2){
      hora1=hora1.split(':'); //jornada de trabajo
      hora2=hora2.split(':'); //horas trabajadas

      hora1=parseInt((hora1[0])*3600)+(parseInt(hora1[1])*60)+parseInt(hora1[1]);
      hora2=parseInt((hora2[0])*3600)+(parseInt(hora2[1])*60)+parseInt(hora2[1]);

      if (hora2 > hora1) {
        return true;
      }else{
        return false;
      }
    }

    function loadAsignacion(){
        webService.asignaciones().promise.then(function(data){
            if (data.success) {
                $scope.asigDeducciones=data.data;
                for (var i = 0; i < $scope.asigDeducciones.length; i++) {
                  if ($scope.asigDeducciones[i].estado=='t') {
                    $scope.asigDeducciones[i].disabled=true;
                    $scope.asigDeducciones[i].asignacion=$scope.asigDeducciones[i].id;
                    //$scope.agregadasAD.asignaciones.push($scope.asigDeducciones[i]);
                  }
                }
            }
        })
    }

    function loadAsignacionEmpleado(cedula){
      var empleado = {};
      empleado.cedula=cedula;
      empleado.fecha1=fecha1;
      empleado.fecha2=fecha2;
      webService.asignacionesEmpleado(empleado).promise.then(function(data){
          if (data.success) {
            var asigEmpleado=data.data;
            for (var i = 0; i < asigEmpleado.length; i++) {
              for (var j = 0; j < $scope.asigDeducciones.length; j++) {
                if (asigEmpleado[i].asignacion==$scope.asigDeducciones[j].id) {
                  $scope.asigDeducciones[j].disabled=true;
                  $scope.agregadasAD.asignaciones.push($scope.asigDeducciones[j]);

                }
              }
            }
             
          }
        });
    }
        
    function loadDeduccion(){
        webService.deducciones().promise.then(function(data){
            if (data.success) {
                $scope.asigDeducciones=data.data;
                for (var i = 0; i < $scope.asigDeducciones.length; i++) {
                  if ($scope.asigDeducciones[i].estado=='t') {
                    $scope.asigDeducciones[i].disabled=true;
                    $scope.asigDeducciones[i].asignacion=$scope.asigDeducciones[i].id;
                    //$scope.newAsigna.asignaciones.push($scope.asigDeducciones[i]);
                  }
                }
            }
        });

    }

    function loadDeduccionEmpleado(cedula){
      var empleado = {};
      empleado.cedula=cedula;
      $scope.agregadasAD.asignaciones=[];
      webService.deduccionesEmpleado(empleado).promise.then(function(data){
          if (data.success) {
            var deducEmpleado=data.data;
            for (var i = 0; i < deducEmpleado.length; i++) {
              for (var j = 0; j < $scope.asigDeducciones.length; j++) {
                if (deducEmpleado[i].deduccion==$scope.asigDeducciones[j].id) {
                  $scope.asigDeducciones[j].disabled=true;
                  $scope.agregadasAD.asignaciones.push($scope.asigDeducciones[j]);

                }
              }
            }
             
          }
        });
    }

    $scope.guardarAD = function (){

        if ($scope.title=='Asignación') {
            webService.asignacionEmpleado($scope.newAsigna).promise.then(function(data){
                if (data.success) {
                    $scope.asigDeducciones=data.data;
                    $mdDialog.cancel();
                    $scope.showSimpleToast(data.msg);//mensaje de guardado
                }
            })
        }else{
            $scope.newAsigna.deducciones=$scope.newAsigna.asignaciones;
            delete $scope.newAsigna.asignaciones;
            webService.deduccionEmpleado($scope.newAsigna).promise.then(function(data){
                if (data.success) {
                    $scope.asigDeducciones=data.data;
                    $mdDialog.cancel();
                    $scope.showSimpleToast(data.msg);//mensaje de guardado
                }
            })
        }
    }

    $scope.listo = function(empleado){
      for (var i = 0; i < $scope.empleados.length; i++) {
        if ($scope.empleados[i].cedula==empleado.cedula){
          $scope.empleados[i].procesado=true;
        }
      }
      guardarEnNomina(empleado.cedula);
    }

    function guardarEnNomina(empleado){
      var nomina={};
      nomina.fecha=moment().format("YYYY-MM-DD");
      nomina.cedula=empleado;
      
      webService.newNomina(nomina).promise.then(function(data){
        if (data.success) {
          $scope.showSimpleToast(data.msg);//mensaje de guardado
        }
      });
    }

    $scope.imprimirRecibo = function(empleado){
      var nomina={};
      nomina.cedula=empleado.cedula;
      console.log(empleado);
      webService.reciboNomina(nomina).promise.then(function(data){
        
      });
    }

    $scope.guardarHoraExtra=function(){
      $scope.totalHoraExtra.fecha=moment().format("YYYY-MM-DD");
      var totalHora=$scope.totalHoraExtra.hora.split(':');
      totalHora=parseInt(totalHora[0])+(parseInt(totalHora[1])/60);
      $scope.totalHoraExtra.hora=totalHora;
      webService.saveHExtra($scope.totalHoraExtra).promise.then(function(data){
        $scope.showSimpleToast(data.msg);
        $mdDialog.cancel();
        $scope.cargarNomina();

      });
    }


    function DialogController($scope, $mdDialog){
       $scope.hide = function() {
         $mdDialog.hide();
       };

       $scope.cancel = function() {
         $mdDialog.cancel();
       };

       $scope.answer = function(answer) {
         $mdDialog.hide(answer);
       };
    }

    $scope.showSimpleToast = function(text) {
      $mdToast.show(
          $mdToast.simple()
              .textContent(text)
              .hideDelay(3000)
              .position('top right')
        );
    }

 });

