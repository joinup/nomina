'use strict';

/**
 * @ngdoc service
 * @name materialAngularApp.webService
 * @description
 * # webService
 * Service in the materialAngularApp.
 */
angular.module('materialAngularApp')
  .factory('webService', ['$http','$q','$rootScope', function($http,$q,$rootScope) {

	var WEBSERVICE_DIR = '../include/webService.php/';
  	//create cancellable requests
  	function createRequest(info){
  		var defer=$q.defer();
  		$rootScope.cargando=false;
		return {
			promise:$http({method:'POST',url:WEBSERVICE_DIR+info.url,data:{'data':info.data}, timeout: defer.promise})
			.then(function(data){
  				$rootScope.cargando=true;
			}),
			cancel:function(reason){
	            defer.resolve(reason);
	        }
		};
  	}
  	//Requests
  	var login = function(user){
		return createRequest({
			url:'login',
			data:user
		});
	};
  	var logout = function(){
		return createRequest({
			url:'logout',
			data:''
		});
	};
	var checkSession = function(){
		return createRequest({
			url:'checkSession',
			data:''
		});
	};

  	var sucursales = function(data){
		return createRequest({
			url:'sucursales',
			data:data
		});
	};
	var newSucursal = function(data){
		return createRequest({
			url:'newSucursal',
			data:data
		});
	};
	var editSucursal = function(data){
		return createRequest({
			url:'editSucursal',
			data:data
		});
	};
	var departamentos = function(data){
		return createRequest({
			url:'departamentos',
			data:data
		});
	};
	var newDepartamentos = function(data){
		return createRequest({
			url:'newDepartamentos',
			data:data
		});
	};
	var editDepartamentos = function(data){
		return createRequest({
			url:'editDepartamentos',
			data:data
		});
	};
	var newSucursalDepartamentos = function(data){
		return createRequest({
			url:'newSucursalDepartamentos',
			data:data
		});
	};
	var empleados = function(data){
		return createRequest({
			url:'empleados',
			data:data
		});
	};
	var empleado = function(data){
		return createRequest({
			url:'empleado',
			data:data
		});
	};
	var newEmpleados = function(data){
		return createRequest({
			url:'newEmpleados',
			data:data
		});
	};
	var editEmpleados = function(data){
		return createRequest({
			url:'editEmpleados',
			data:data
		});
	};

	var newCargo = function(data){
		return createRequest({
			url:'newCargo',
			data:data
		});
	};

	var editCargo = function(data){
		return createRequest({
			url:'editCargo',
			data:data
		});
	};

	var cargos = function(data){
		return createRequest({
			url:'cargos',
			data:data
		});
	};

	var asignaciones = function(data){
		return createRequest({
			url:'asignaciones',
			data:data
		});
	};

	var deducciones = function(data){
		return createRequest({
			url:'deducciones',
			data:data
		});
	};

	var newAsignacion = function(data){
		return createRequest({
			url:'newAsignacion',
			data:data
		});
	};
	
	var newDeduccion = function(data){
		return createRequest({
			url:'newDeduccion',
			data:data
		});
	};

	var editAsignacion = function(data){
		return createRequest({
			url:'editAsignacion',
			data:data
		});
	};
	
	var editDeduccion = function(data){
		return createRequest({
			url:'editDeduccion',
			data:data
		});
	};
	
	var asistencia = function(data){
		return createRequest({
			url:'asistencia',
			data:data
		});
	};
	var configuracion = function(data){
		return createRequest({
			url:'configuracion',
			data:data
		});
	};
	
	var editConfig = function(data){
		return createRequest({
			url:'editConfig',
			data:data
		});
	};
	
	var newFeriado = function(data){
		return createRequest({
			url:'newFeriado',
			data:data
		});
	};
	
	var feriados = function(data){
		return createRequest({
			url:'feriados',
			data:data
		});
	};
	
	var asignacionEmpleado = function(data){
		return createRequest({
			url:'asignacionEmpleado',
			data:data
		});
	};
	
	var deduccionEmpleado = function(data){
		return createRequest({
			url:'deduccionEmpleado',
			data:data
		});
	};
	
	var asignarDepartamentoSucursal = function(data){
		return createRequest({
			url:'asignarDepartamentoSucursal',
			data:data
		});
	};
	
	var asistenciaEmpleado = function(data){
		return createRequest({
			url:'asistenciaEmpleado',
			data:data
		});
	};
	
	var newAsistencia = function(data){
		return createRequest({
			url:'newAsistencia',
			data:data
		});
	};
	
	var nomina = function(data){
		return createRequest({
			url:'nomina',
			data:data
		});
	};
	
	var newNomina = function(data){
		return createRequest({
			url:'newNomina',
			data:data
		});
	};
	var asistenciaNomina = function(data){
		return createRequest({
			url:'asistenciaNomina',
			data:data
		});
	};
	var reciboNomina = function(data){
		return createRequest({
			url:'reciboNomina',
			data:data
		});
	};
	var usuarios = function(data){
		return createRequest({
			url:'usuarios',
			data:data
		});
	};
	var newUsuario = function(data){
		return createRequest({
			url:'newUsuario',
			data:data
		});
	};
	var editUsuario = function(data){
		return createRequest({
			url:'editUsuario',
			data:data
		});
	};

	var horario = function(data){
		return createRequest({
			url:'horario',
			data:data
		});
	};
	var editHorario = function(data){
		return createRequest({
			url:'editHorario',
			data:data
		});
	};
	var asistEmpleado = function(data){
		return createRequest({
			url:'asistEmpleado',
			data:data
		});
	};
	
	var asignacionesEmpleado = function(data){
		return createRequest({
			url:'asignacionesEmpleado',
			data:data
		});
	};
	var deduccionesEmpleado = function(data){
		return createRequest({
			url:'deduccionesEmpleado',
			data:data
		});
	};
	
	var tiempoTrabajado = function(data){
		return createRequest({
			url:'tiempoTrabajado',
			data:data
		});
	};
	
	var saveHExtra = function(data){
		return createRequest({
			url:'saveHExtra',
			data:data
		});
	};
	

return {
		login: function(user){return login(user);},
		logout: function(){return logout();},
		checkSession: function(){return checkSession();},
		sucursales: function(data){return sucursales(data);},
		newSucursal: function(data){return newSucursal(data);},
		editSucursal: function(data){return editSucursal(data);},
		departamentos: function(data){return departamentos(data);},
		newDepartamentos: function(data){return newDepartamentos(data);},
		editDepartamentos: function(data){return editDepartamentos(data);},
		newSucursalDepartamentos: function(data){return newSucursalDepartamentos(data);},
		empleado: function(data){return empleado(data);},
		empleados: function(data){return empleados(data);},
		newEmpleados: function(data){return newEmpleados(data);},
		editEmpleados: function(data){return editEmpleados(data);},
		newCargo: function(data){return newCargo(data);},
		editCargo: function(data){return editCargo(data);},
		cargos: function(data){return cargos(data);},
		asignaciones: function(data){return asignaciones(data);},
		deducciones: function(data){return deducciones(data);},
		newAsignacion: function(data){return newAsignacion(data);},
		newDeduccion: function(data){return newDeduccion(data);},
		editAsignacion: function(data){return editAsignacion(data);},
		editDeduccion: function(data){return editDeduccion(data);},
		asistencia: function(data){return asistencia(data);},
		configuracion: function(data){return configuracion(data);},
		editConfig: function(data){return editConfig(data);},
		newFeriado: function(data){return newFeriado(data);},
		feriados: function(data){return feriados(data);},
		asignacionEmpleado: function(data){return asignacionEmpleado(data);},
		deduccionEmpleado: function(data){return deduccionEmpleado(data);},
		asignarDepartamentoSucursal: function(data){return asignarDepartamentoSucursal(data);},
		asistenciaEmpleado: function(data){return asistenciaEmpleado(data);},
		newAsistencia: function(data){return newAsistencia(data);},
		nomina: function(data){return nomina(data);},
		newNomina: function(data){return newNomina(data);},
		asistenciaNomina: function(data){return asistenciaNomina(data);},
		reciboNomina: function(data){return reciboNomina(data);},
		usuarios: function(data){return usuarios(data);},
		newUsuario: function(data){return newUsuario(data);},
		editUsuario: function(data){return editUsuario(data);},
		horario: function(data){return horario(data);},
		editHorario: function(data){return editHorario(data);},
		asistEmpleado: function(data){return asistEmpleado(data);},
		asignacionesEmpleado: function(data){return asignacionesEmpleado(data);},
		deduccionesEmpleado: function(data){return deduccionesEmpleado(data);},
		tiempoTrabajado: function(data){return tiempoTrabajado(data);},
		saveHExtra: function(data){return saveHExtra(data);},
	};
    
  }]);
 