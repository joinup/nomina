<?php 
    // reference the Dompdf namespace
require_once 'dompdf/autoload.inc.php';//libreria de importacion del pdf
use Dompdf\Dompdf;

function newPdf($body,$title,$path='') {
    // instantiate and use the dompdf class
    $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); 
    $path = ($path=='')?'listado.pdf':$path;
    $header = '<div class="header">
        <div style="width:60%; height:20%;">
            <img src="../app/img/invermata.png" width="220px"></br>
        </div></br>
        <div style="width:34%;float:right"><h5 style="text-align:right">'.$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y').' ' .date('h:m').'</h5></div>
        <h2 style="text-align:center;margin-top:80px">'.$title.'</h2>

    </div>';
    $content=' 
        <!doctype html> 
        <html> 
        <head> 
            <link rel="stylesheet" href="style.css" type="text/css" /> 
        </head> 
        <body>' 
            .$header.$body. 
        '</body> 
        </html>'; 
    $dompdf = new Dompdf();
    $dompdf->loadHtml($content);

    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4');

    // Render the HTML as PDF
    $dompdf->render();
    $canvas = $dompdf->get_canvas();
    //$font = Font_Metrics::get_font("helvetica", "bold");
    $canvas->page_text(275, 810, "Página: {PAGE_NUM} de {PAGE_COUNT}", '', 8, array(0,0,0));


    // Output the generated PDF to Browser
    //$dompdf->stream($path,array('Attachment'=>0));
    file_put_contents($path, $dompdf->output());
    //$dompdf->save($path);
}   


//newPdf();

 ?>