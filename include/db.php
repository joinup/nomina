<?php 

	require_once ("constant.php");
	class DB{
		private $con=null;
		private $result=null;
		private $lastTable=null;
		private $sqlSaver=null;
		function __construct(){
			$this->con = pg_connect("host=".HOST_DB_PG." port=".PORT_DB_PG." dbname=".NAME_DB_PG."  user=".USER_DB_PG." password=".PASSWORD_DB_PG."");
			if(!$this->con)
				exit('connection failed');

		}
		function lastError(){
			return pg_last_error($this->con);
		}
		
		/*
			Query and get the first row
		*/
		function queryRow($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return FALSE;
			return pg_fetch_assoc($this->result);
		}

		function queryAll($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return FALSE;
			return pg_fetch_all($this->result);
		}
		/*
			begin transaction
		*/
		function begin(){
			return $this->query("BEGIN");
		}
		/*
			finish transaction
		*/
		function finish(){
			return $this->query("COMMIT");
		}
		/*
			rollback transaction
		*/
		function rollback(){
			return $this->query("ROLLBACK");
		}
		private function query($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return $this->getError();
			return TRUE;
		}

		function insertRow($table,$values){
			//$this->lastTable=$table;
			$result =pg_insert($this->con,$table,$values);
			return $result;
			//return pg_affected_rows($result);
		}
		
		/*
			Update table
		*/
		function updateRows($table,$values,$conditions){
			$result =pg_update($this->con,$table,$values,$conditions);
			return $result;
		}
		function login($user){
			$username=$this->queryRow("SELECT * FROM propietario where cedula='".$user['cedula']."'");
			if ($username) {
				if ($username['clave']==$user['clave']) {
					return array('success'=>true,'msg'=>'Bienvenido '.$username['nombre']." ".$username['apellido'],'data'=>$username);
				} else {
					return array('success'=>false,'msg'=>'Clave invalida','data'=>null);
				}
			} else {
				return array('success'=>false,'msg'=>'El propietario no esta registrado','data'=>null);
			}
			
		}

		function cargarProfile($email){
			return $this->queryRow("SELECT * FROM users where email='$email'");
		}
		function propiedades($data){
			$where=($data['numero'])?" where numero=".$data['numero']:'';
			return $this->queryAll("SELECT * from propiedad".$where);
		}

		function propiedadesActivas($data){
			return $this->queryAll("SELECT * from propiedad where status_ocupacion='desocupada'");
		}
		function propietario($data){
			$where=($data['cedula'])?" and a.cedula='".$data['cedula']."'":'';
			return $this->queryAll("SELECT a.*,b.numero_casa,sum(case c.estado when 'pendiente' then 1 else 0 end) as deuda from propietario a inner join alquiler  b on a.cedula=b.cedula left join mensualidad c on b.numero_casa=c.numero_casa where a.estado='activo' ".$where." and b.fecha_fin  is null  group by a.cedula,b.numero_casa");
		}

		function vermensualidad($data){
			$where=($data['cedula'])?" and a.cedula = '".$data['cedula']."'":'';
			return $this->queryAll("SELECT c.*, a.numero_casa,
				a.*,a.id as mensualidad
				from mensualidad a inner join alquiler b on a.numero_casa=b.numero_casa
 				inner join propietario c on b.cedula=c.cedula 
				where mes='".$data['mes']."' and ano='".$data['anio']."' and a.estado='".$data['tipo']."' ".$where);
		}

		function propiedad_propietarios($data){
			$where=($data['cedula'])?" where cedula='".$data['cedula']."'":'';
			$tabla1=$this->queryAll("SELECT * from propietario".$where);
			$tabla2=$this->queryAll("SELECT * from alquiler a inner join propiedad b on a.numero_casa=b.numero");
			//return $tabla2;
			return $this->unir($tabla1,$tabla2,'cedula','cedula','propiedades');
		}
		function registrarPropietario($data){
			//return $data;
			$registro=$this->insertRow('propietario',$data['propietario']);
			$pp=$this->queryRow("INSERT INTO alquiler (numero_casa,cedula,tipo) values ('".$data['propiedad']['numero_casa']."','".$data['propietario']['cedula']."','".$data['propiedad']['tipo']."')");
			if (isset($data['propiedad']['numero_casa'])) {
				$this->queryRow("UPDATE propiedad set status_ocupacion='ocupada' WHERE numero='".$data['propiedad']['numero_casa']."' ");
			}
			return $registro ;
		}
		function eliminarPropietario($data){
			$values=array('estado'=>'inactivo');
			$conditions=array('cedula' => $data['cedula']);
			$this->queryRow("UPDATE alquiler set fecha_fin=CURRENT_DATE where cedula='".$data['cedula']."'");
			$this->queryRow("UPDATE propiedad set status_ocupacion='desocupada' where numero=(SELECT numero_casa FROM alquiler where cedula='".$data['cedula']."')");
			return $this->updateRows('propietario',$values,$conditions);

		}
		function editarPropietario($data){
			unset($data['numero_casa']);
			$values=$data;
			$conditions=array('cedula' => $data['cedula']);
			return $this->updateRows('propietario',$values,$conditions);
		}		
		
		function unir($tabla1,$tabla2,$id1,$id2,$union){
			if (gettype($tabla1)=='string')
				$tabla1 = $this->queryAll("SELECT * from ".$tabla1);
			if (gettype($tabla2)=='string')
				$tabla2 = $this->queryAll("SELECT * from ".$tabla2);
			$aux = array();
			foreach ($tabla1 as $key => $item1) {
				foreach ($tabla2 as $key => $item2) {
					if ($item1[$id1]==$item2[$id2]) {
						$item1[$union]=$item2;
					}
				}
				$aux[]=$item1;
			}
			return $aux;
		}

		////Propiedad

		function registrarPropiedad($data){
			$propiedad=$this-> insertRow('propiedad',$data);
			return array('success'=>true,'msg'=>'propiedad insertada','data'=>null);
		}

		function editarPropiedad($data){
			
			$propiedadEdit=$this->updateRows("propiedad", $data, array('numero'=>$data['numero']));
			if ($data['status_ocupacion']=="desocupada") {
				$alquiler=$this->queryRow("UPDATE alquiler set fecha_fin=CURRENT_DATE WHERE numero_casa='".$data['numero']."'");
				$cedula=$this->queryRow("SELECT cedula from alquiler where numero_casa='".$data['numero']."'");
				$cedula=$cedula['cedula'];
				$this->queryRow("UPDATE propietario set estado='inactivo' where cedula='".$cedula."'");
			}
			return $propiedadEdit;
		}


		////cuenta/////////////////////
		function cuentas($data){
			$where=($data['num_cuenta'])?" where num_cuenta='".$data['num_cuenta']."'":'';
			//$where=($data['ci'])?" where id=".$data['id']:'';
			return $this->queryAll("SELECT * from cuenta".$where);
		}

		function registrarCuenta($data){
			$pcuenta=$this->insertRow('cuenta',$data);
			return array('success'=>true,'msg'=>'cuenta insertada','data'=>null);
		}

		function eliminarCuenta($data){
			$cuentaE=$this->queryRow("DELETE FROM cuenta where num_cuenta='".$data['num_cuenta']."'");
			return $cuentaE;
		}

		function editarCuenta($data){
			$cuentaEdit=$this->updateRows("cuenta", $data, array('num_cuenta'=>$data['num_cuenta']));
			return $cuentaEdit;
		}

		//Gastos/////////////

		function registrarGasto($data){
			return $this->insertRow('gastos_condominio',$data);
			
		}

		function registrarConcepto($data){
			$concepto=$this->insertRow('concepto',$data);
			return array('success'=>true,'msg'=>'concepto insertado','data'=>null);
		}

		function conceptos($data){
			$where=($data['id'])?" and id=".$data['id']:'';
			//$where=($data['ci'])?" where id=".$data['id']:'';
			return $this->queryAll("SELECT * from concepto where estado='activo'".$where);
		}

		function proveedores($data){
			return $this->queryAll("SELECT * from proveedor");
		}

		function registrarProveedor($data){
			return $prov=$this->insertRow('proveedor',$data);
			
		}

		function gastos($data){
			/*$valor=explode("-", $data);
			if ($valor[0] == 02) {
				$fecha2=$valor[1]."-".$valor[0]."-29";
			}elseif ($valor[0] == 01 | $valor[0] == 03 | $valor[0] == 05 | $valor[0] == 07 | $valor[0] == 08 | $valor[0] == 10 | $valor[0] == 12) {
				$fecha2=$valor[1]."-".$valor[0]."-31";
			}else{
				$fecha2=$valor[1]."-".$valor[0]."-30";
			}*/

			//$fecha1=$valor[1]."-".$valor[0]."-01";
		   // return "SELECT a.id, a.descripcion, b.nombre, proveedor, fecha, monto from gastos_condominio a left join concepto b on a.concepto=b.id left join proveedor c on a.proveedor=c.cedula WHERE fecha BETWEEN '".$data['startdate']."' AND '".$data['enddate']."'";	
			return $this->queryAll("SELECT a.id, a.descripcion, b.nombre, c.nombre as proveedor, fecha, monto from gastos_condominio a left join concepto b on a.concepto=b.id left join proveedor c on a.proveedor=c.cedula WHERE fecha BETWEEN '".$data['startdate']."' AND '".$data['enddate']."'");
				
		}

		function desactivarGasto($data){
			$editG=$this->queryRow("DELETE FROM gastos_condominio where id='".$data['id']."'");
			return $editG;
		}

		function editarGasto($data){
			$editG=$this->queryRow("UPDATE gastos_condominio SET MONTO=".$data['monto']." where id=".$data['id']."");
			return $editG;
		}
		function calcularGastos($data){
			$fechaInicio = $data['startdate'];
			$fechaFinal = $data['enddate'];
			return $suma = $this->queryRow("SELECT sum(monto) as total from gastos_condominio 
				where fechaRegistro>='".$fechaInicio."' and fechaRegistro<='".$fechaFinal."'");
		}
		function cantCasas(){
			return $this->queryRow("SELECT count(numero) as cantidad from propiedad where status_ocupacion = 'ocupada'");
		}
		function nuevaMensualidad($monto,$mes,$anio){
			$a = $this->queryRow("SELECT * from mensualidad where mes='$mes' and ano='$anio'");
			//if ($a) {
				//$conditions=array('mes'=>$mes,'ano'=>$anio);
				//$values=array('monto'=>$monto);
				//return $this->updateRows('mensualidad',$values,$conditions);
			//}
			return $this->queryRow("INSERT INTO mensualidad (numero_casa,fecha,mes,ano,monto,estado) (select numero,current_date,'".$mes."','".$anio."',".$monto.",'pendiente' from propiedad where status_ocupacion='ocupada')");
		}
		function insertFondo($monto,$fecha){
			$concepto = $this->queryRow("SELECT * FROM concepto where nombre='Fondo de Reserva'");
			if ($concepto[nombre]=='') {
				$this->queryRow("INSERT into concepto (nombre,tipo) values ('Fondo de Reserva','fijo')");
			}
				$id=$this->queryRow("SELECT id FROM concepto where nombre='Fondo de Reserva'");
				$this->queryRow("INSERT into gastos_condominio (descripcion,concepto,fecha,monto) values ('Fondo de reserva',".$id['id'].",'".$fecha."',".$monto.")");
			
		}
		function insertPago($data){
			return $this->insertRow('pago',$data);
		}
		function updateMensualidad($data){
			$id = $data['mensualidad'];
			$mensualidad = $this->queryRow("SELECT * from mensualidad where id=".$id);
			$estado = ($_SESSION['role']!='propietario')?'pagado':'espera';
			$propietario = $this->queryRow("SELECT abono from propietario where cedula='".$data['cedula']."'");
			$monto = (real)$data['monto'] + (real)$propietario['abono'];
			$montoMen = (real)$mensualidad['monto'];
			if ($monto>=$montoMen) {
				$this->queryRow("UPDATE mensualidad set estado='".$estado."' where id=$id");
					$this->queryRow("UPDATE propietario set abono=".($monto-$montoMen)." where  cedula='".$data['cedula']."'");
			} else {
				$this->queryRow("UPDATE propietario set abono=".$monto." where cedula='".$data['cedula']."'");
				/*$this->queryRow("UPDATE mensualidad set monto=monto-".$data['monto']." where id=$id");*/
				
			}
			return true;
		}
		function verPagos($data){
			return $this->queryAll("SELECT a.*,b.banco from pago a left join cuenta b on a.num_cuenta=b.num_cuenta where mensualidad=".$data['id']);
		}



///////////////////////////junta/////////////
		function junta($data){
			return $this->queryAll("SELECT * from junta where estado='activa'");
		}

		function miembrosJunta($data){
			return $this->queryAll("SELECT b.nombre, a.cedula, a.cargos from junta_propietario a inner join propietario b on a.cedula=b.cedula where a.estado='activo';");
		}

		function registrarJunta($data){
			$this->queryRow(" UPDATE junta set estado='inactiva',  fecha_fin=CURRENT_DATE WHERE id=(select max(id) from junta);");
			$juntaR = $this->queryRow("INSERT INTO JUNTA (nombre) values ('".$data['nombre']."')");
			
			$this->queryRow("UPDATE junta_propietario SET estado='inactivo', fecha_fin=CURRENT_DATE");	
			
			return $juntaR;
		}

		function guardarEnJuntaP($data){
			$id=$this->queryRow("SELECT id from junta WHERE estado='activa'");
			$id=$id['id'];
			$this->queryRow("INSERT INTO junta_propietario (cedula,junta,fecha_inicio,cargos,estado) values ('".$data['propietario']."',".$id.",CURRENT_DATE,'".$data['cargo']."','activo')");

		}

		function otroMiembro($data){
			$id=$this->queryRow("SELECT id from junta WHERE estado='activa'");
			$id=$id['id'];
			$cedula=$this->queryRow("SELECT cedula from junta_propietario where cedula='".$data['propietario']."' AND junta=".$id." ");
			if (isset($cedula['cedula'])) {
				$this->queryRow("UPDATE junta_propietario set fecha_inicio=CURRENT_DATE, estado='activo', cargos='".$data[cargo]."' WHERE cedula='".$data['propietario']."' AND junta=".$id."");
			}
			else{
				$this->queryRow("INSERT INTO junta_propietario (cedula,junta,fecha_inicio,cargos,estado) values ('".$data['propietario']."',".$id.",CURRENT_DATE,'".$data['cargo']."','activo')");
			}

		}

		function nuevaActividad($data){
			$id=$this->queryRow("SELECT id from junta WHERE estado='activa'");
			$id=$id['id'];
			$actividad=$this->queryRow("INSERT into actividades (junta,descripcion,fecha,presupuesto,estado) values (".$id.",'".$data['descripcion']."','".$data['fecha']."','".$data['presupuesto']."','Pendiente')");
			return $actividad;
		}
			
		function veractividad($data){
			return $this->queryAll("SELECT id,fecha,descripcion,presupuesto,estado from actividades where junta = (select id from junta where estado='activa')");
		}

		function eliminarMiembro($data){
			return $this->queryRow("UPDATE junta_propietario set estado='inactivo',fecha_fin=CURRENT_DATE where cedula='".$data['cedula']."'");
		}

		function actualizarActividad($data){
			return $this->queryRow("UPDATE actividades set estado='".$data['estado']."' where id=".$data['id']."");
		}

		function detalle($data){
			$fecha = date('Y-m');
		$nuevafecha = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m' , $nuevafecha );
			return $this->queryAll("SELECT b.nombre as proveedor,sum(a.monto) as monto from proveedor b right join gastos_condominio a on a.proveedor=b.cedula where a.fecharegistro>='".$nuevafecha."-01' and a.fecharegistro<'".date('Y-m')."-01' and a.descripcion<>'Fondo de reserva' group by b.cedula");
		}

	}
 ?>