<?php

/*
formato del JSON: '{data:{"key":"value"}}'

*/

//ini_set("session.use_trans_sid","0");
//ini_set("session.use_only_cookies","1");
/*session_name('festigame');*/
session_name('condominio');
if(!isset($_SESSION))
        session_start();
require_once("db.php");
define("WS_FOLDER", "webService/");

class JSON_WebService {
    private $methods, $args, $strcall;
    public function __construct($rawData) {
        $this->strcall = str_replace($_SERVER["SCRIPT_NAME"]."/", "", $_SERVER["REQUEST_URI"]);
        //echo $rawData;
        $this->args = $rawData;
        $this->methods = array();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json; charset=utf-8');
    }

    public function Register($name) {
        $this->methods[$name] = true;
    }

    public function Remove($name) {
        $this->methods[$name] = false;
    }

    private function call($name, $args) {
        if ($this->methods[$name] == true) {
            if($args==null) $args=array();
            $result = call_user_func_array($name,$args);
            return json_encode($result);
        }
    }

    function start() {
        try{
            if(!function_exists($this->strcall))
                throw new Exception("Function '".$this->strcall."' does not exist.");
            if (!$this->methods[$this->strcall])
                throw new Exception("Access denied for function '".$this->strcall."'.");

            header("HTTP/1.0 200 OK");
            print $this->call($this->strcall, json_decode($this->args,true));
        }
        catch(Exception $e){
            header("HTTP/1.0 500 Internal server error");
            print json_encode(
                array(
                    "message" => $e->getMessage(),
                    "code" => $e->getCode(),
                    "file" => $e->getFile(),
                    "line" => $e->getLine(),
                    "stackTrace" => $e->getTrace(),
                    "status" => array("message" => "Internal server error", "code" => "500")
                )
            );
        }
    }
}
function error500()
{
    header("HTTP/1.0 500 Internal server error");
    //exit();
}
function onlyAdmin()
{
    if(!isset($_SESSION['user_role']) || $_SESSION['user_role']!='admin'){
        error500();
        exit();
    }
}



//Obtiene el contenido de la solicitud POST
$HTTP_RAW_POST_DATA = file_get_contents("php://input");
//Instancia de la clase JSON_WebService
$functionName = str_replace($_SERVER["SCRIPT_NAME"]."/", "", $_SERVER["REQUEST_URI"]);
$server = new JSON_WebService($HTTP_RAW_POST_DATA);
//Registra los metodos del servicio web
//declare functions to webServer
require (WS_FOLDER."login.php");
require (WS_FOLDER."sucursal.php");
require (WS_FOLDER."departamento.php");
require (WS_FOLDER."asignacion_deduccion.php");
require (WS_FOLDER."cargo.php");
require (WS_FOLDER."empleado.php");
require (WS_FOLDER."asistencia.php");
require (WS_FOLDER."configuracion.php");
require (WS_FOLDER."entrada_salida.php");
require (WS_FOLDER."nomina.php");
require (WS_FOLDER."usuario.php");


$server->start();




//Define los metodos del servicio web


