<?php 
	$server->register("sucursales");
	$server->register("newSucursal");
	$server->register("editSucursal");

	function sucursales($estado){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM sucursal where estado='".$estado['estado']."'");

		if ($resp==true) {
			$sucursal_depa=array();
			for ($i=0; $i < count($resp); $i++) { 
				$sucursal_depa[$i]['id']=$resp[$i]['id'];
				$sucursal_depa[$i]['nombre']=$resp[$i]['nombre'];
				$sucursal_depa[$i]['direccion']=$resp[$i]['direccion'];
				$sucursal_depa[$i]['telefono']=$resp[$i]['telefono'];
				$sucursal_depa[$i]['estado']=$resp[$i]['estado'];
				$departamentos = $db->queryAll("SELECT id,nombre from departamento a inner join sucursal_departamento b on a.id=b.departamento where b.sucursal=".$resp[$i]['id']);
				$sucursal_depa[$i]['departamentos']=$departamentos;

			}
			return array('success'=>true,'msg'=>"Sucursales registradas", 'data'=>$sucursal_depa);
		}
		return array('success'=>false,'msg'=>"No hay sucursales registradas");
	}

	function newSucursal($sucursal){
		$db=new DB();

		$resp = $db->insertRow("sucursal",$sucursal);
		if ($resp) {
			return array('success'=>true,'msg'=>"Sucursal registrada con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar la sucursal");
		}
	}

	function editSucursal($sucursal){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM sucursal where id=".$sucursal['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"La sucursal no se encuentra registrada");
		}else{
			$condition = array('id' =>  $sucursal['id']);
			$resp = $db->updateRows("sucursal",$sucursal,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"Sucursal modificada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar la sucursal");
			}
		}
	}
 ?>