<?php 
	$server->register("configuracion");
	$server->register("editConfig");
	$server->register("newFeriado");
	$server->register("feriados");
	$server->register("horario");
	$server->register("editHorario");

	function configuracion(){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM configuracion");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"configuración registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay asignaciones registradas");
	}

	function editConfig($config){
		$db=new DB();

		$existe=configuracion();

		if ($existe['success']==false) {
			if (isset($config['sueldo_minimo'])) {
				$resp= $db->queryRow("INSERT into configuracion (sueldo_minimo) values (".$config['sueldo_minimo'].")");
			}

			if (isset($config['unidad_tributaria'])) {
				$resp= $db->queryRow("INSERT INTO configuracion (unidad_tributaria) values (".$config['unidad_tributaria'].")");

				$ticket=($config['unidad_tributaria'] * 12) * 30;

				$db->queryRow("INSERT INTO configuracion (cestaticket) values ($ticket)");
			}
		}else{
			if (isset($config['sueldo_minimo'])) {
				$resp= $db->queryRow("UPDATE configuracion set sueldo_minimo=".$config['sueldo_minimo']);
			}

			if (isset($config['unidad_tributaria'])) {
				$resp= $db->queryRow("UPDATE configuracion set unidad_tributaria=".$config['unidad_tributaria']);
				$ticket=($config['unidad_tributaria'] * 12) * 30;
				$db->queryRow("UPDATE configuracion SET cestaticket=$ticket");

			}
		}


		if (!$resp) {
			return array('success'=>true,'msg'=>"Editado con exito");
		}else{
			return array('success'=>false,'msg'=>"Problemas al editar");
		}

	}

	function newFeriado($feriado){
		$db=new DB();

		$resp = $db->insertRow("feriados",$feriado);
		if ($resp) {
			return array('success'=>true,'msg'=>"Día feriado registrado con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar el día de feriado");
		}
	}

	function feriados(){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM feriados order by fecha");
		if ($resp) {
			return array('success'=>true,'msg'=>"Días feriados", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"No hay días feriados");
		}
	}
	
	function horario(){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM horario");
		if ($resp) {
			return array('success'=>true,'msg'=>"Horario de trabajo", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"No hay horario");
		}
	}

	function editHorario($horario){
		$db=new DB();

		$existe=horario();

		if ($existe['success']==false) {
			$resp = $db->queryRow("INSERT INTO horario (hora_entrada,hora_salida,jornada) values ('".$horario['hora_entrada']."','".$horario['hora_salida']."','".$horario['jornada']."')");
		}else{
			$resp = $db->queryRow("UPDATE horario set hora_entrada='".$horario['hora_entrada']."', hora_salida='".$horario['hora_salida']."', jornada='".$horario['jornada']."'");
		}

		if (!$resp) {
			return array('success'=>true,'msg'=>"Horario editado", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"Error en horario");
		}
	}



 ?>