<?php 
	$server->register("empleados");
	$server->register("empleado");
	$server->register("newEmpleados");
	$server->register("editEmpleados");

	function empleado($cedula){
		$db=new DB();

		$resp = $db->queryAll("SELECT b.nombre as nombre_cargo, a.* FROM empleado a left join cargo b on a.cargo=b.id where a.estado='activo' and a.cedula='$cedula'");
		if ($resp==true) {
			return array('success'=>true,'msg'=>"Datos del empleado", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"Esta cedula no esta registrada");
		}
	}

	function empleados($estado){
		$db=new DB();


		$where = ($_SESSION['tipo']=='admin')?" and a.sucursal=".$_SESSION['sucursal']:'';
		$resp = $db->queryAll("SELECT b.nombre as nombre_cargo, a.*, c.nombre as suc, d.nombre as nombre_dep FROM empleado a left join cargo b on a.cargo=b.id inner join sucursal c on c.id=a.sucursal inner join departamento d on d.id=a.departamento where a.estado='".$estado['estado']."'".$where);
		if ($resp==true) {
			return array('success'=>true,'msg'=>"empleados registrados", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"No hay empleados registrados");
		}
	}
	function newEmpleados($empleado){
		$db=new DB();
		$resp = $db->queryAll("SELECT * FROM empleado where cedula='".$empleado['cedula']."'");
		if ($resp==true) {
			return array('success'=>false,'msg'=>"el empleado ya esta registrado");
		}
		$resp = $db->insertRow("empleado",$empleado);
		if ($resp) {
			return array('success'=>true,'msg'=>"empleado registrado con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar el empleado");
		}
	}
	
	function editEmpleados($empleado){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM empleado where cedula='".$empleado['cedula']."'");
		if (!$resp) {
			return array('success'=>false,'msg'=>"el empleado no se encuentra registrado");
		}else{
			$condition = array('cedula' =>  $empleado['cedula']);
			$resp = $db->updateRows("empleado",$empleado,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"empleado modificado con exito");
			}else{
				return array('success'=>false,'msg'=>"error a modificar el empleado");
			}
		}
	}

 ?>