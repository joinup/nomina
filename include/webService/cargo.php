<?php 
	$server->register("cargos");
	$server->register("newCargo");
	$server->register("editCargo");

	function cargos($cargo){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM cargo where estado='".$cargo['estado']."'");
		if ($resp==true) {
			return array('success'=>true,'msg'=>"cargos registrados", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay cargos registrados");
	}
	function newCargo($cargo){
		$db=new DB();
		
		$resp = $db->insertRow("cargo",$cargo);
		if ($resp) {
			return array('success'=>true,'msg'=>"cargo registrado con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar el cargo");
		}
	}
	function editCargo($cargo){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM cargo where id=".$cargo['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"el cargo no se encuentra registrado");
		}else{
			$condition = array('id' =>  $cargo['id']);
			$resp = $db->updateRows("cargo",$cargo,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"cargo modificado con exito");
			}else{
				return array('success'=>false,'msg'=>"error a modificar el cargo");
			}
		}
	}
 ?>