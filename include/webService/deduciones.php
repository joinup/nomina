<?php 
	$server->register("deducciones");
	$server->register("newDeduciones");
	$server->register("editDeduciones");

	function deducciones(){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM deduccion");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"deducciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay deducciones registradas");
	}

	function newDeduciones($deduccion){
		$db=new DB();

		$resp = $db->insertRow("deduccion",$deduccion);
		if ($resp) {
			return array('success'=>true,'msg'=>"deducción registrada con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar la deducción");
		}
	}

	function editDeduciones($deduccion){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM deduccion where id=".$deduccion['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"La deduccion no se encuentra registrada");
		}else{
			$condition = array('id' =>  $deduccion['id']);
			$resp = $db->updateRows("deduccion",$deduccion,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"deducción modificada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar la deduccion");
			}
		}
	}
 ?>