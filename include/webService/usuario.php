<?php 
	$server->register("usuarios");
	$server->register("newUsuario");
	$server->register("editUsuario");

	function usuarios($estado){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT a.cedula, a.nombre, a.tipo, a.sucursal, b.nombre as sucursal_nombre FROM usuario a left join sucursal b on a.sucursal=b.id");

		if ($resp) {
			return array('success'=>true,'msg'=>"usuarios registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay usuarios registradas");
	}

	function newUsuario($usuario){
		$db=new DB();
		$resp = $db->queryAll("SELECT * FROM usuario where cedula='".$usuario['cedula']."'");
		if (!$resp) {
			$resp = $db->insertRow("usuario",$usuario);
			if ($resp) {
				return array('success'=>true,'msg'=>"Usuario registrada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al registrar el usuario");
			}
		}else{
			return array('success'=>false,'msg'=>"La cedula ya se encuentra registrada");
		}
	}

	function editUsuario($usuario){
		unset($usuario['sucursal_nombre']);
		$db=new DB();
		$resp = $db->queryAll("SELECT * FROM usuario where cedula='".$usuario['cedula']."'");
		if (!$resp) {
			return array('success'=>false,'msg'=>"El usuario no se encuentra registrado");
		}else{
			$condition = array('cedula' =>  $usuario['cedula']);
			$resp = $db->updateRows("usuario",$usuario,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"usuario modificado con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar el usuario");
			}
		}
	}


?>