<?php 
	$server->register("asignaciones");
	$server->register("deducciones");
	$server->register("newAsignacion");
	$server->register("newDeduccion");
	$server->register("editAsignacion");
	$server->register("editDeduccion");
	$server->register("asignacionesEmpleado");
	$server->register("deduccionesEmpleado");


	function asignaciones(){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM asignacion");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"Asignaciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay asignaciones registradas");
	}
	
	function deducciones(){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM deduccion");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"Deducciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay deducciones registradas");
	}

	function newAsignacion($asignacion){
		$db=new DB();

		$resp = $db->insertRow("asignacion",$asignacion);
		if ($resp) {
			return array('success'=>true,'msg'=>"Asignación registrada con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar la asignación");
		}
	}

	function newDeduccion($deduccion){
		$db=new DB();

		$resp = $db->insertRow("deduccion",$deduccion);
		if ($resp) {
			return array('success'=>true,'msg'=>"Deducción registrada con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar la deducción");
		}
	}

	function editAsignacion($asignacion){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM asignacion where id=".$asignacion['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"La asignacion no se encuentra registrada");
		}else{
			$condition = array('id' =>  $asignacion['id']);
			$resp = $db->updateRows("asignacion",$asignacion,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"Asignacion modificada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar la asignacion");
			}
		}
	}

	function editDeduccion($deduccion){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM deduccion where id=".$deduccion['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"La deduccion no se encuentra registrada");
		}else{
			$condition = array('id' =>  $deduccion['id']);
			$resp = $db->updateRows("deduccion",$deduccion,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"Deducción modificada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar la dedución");
			}
		}
	}

	function asignacionesEmpleado($empleado){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM asignacion_empleado where cedula='".$empleado['cedula']."' and fecha BETWEEN '".$empleado['fecha1']."' and '".$empleado['fecha2']."'");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"asignaciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay asignaciones registradas");
	}

	function deduccionesEmpleado($empleado){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM deduccion_empleado where cedula='".$empleado['cedula']."'");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"deducciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay deducciones registradas");
	}


 ?>