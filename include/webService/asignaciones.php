<?php 
	$server->register("asignaciones");
	$server->register("newAsignacion");
	$server->register("editAsignacion");

	function asignaciones($estado){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT * FROM asignacion");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"asignaciones registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay asignaciones registradas");
	}

	function newAsignacion($asignacion){
		$db=new DB();

		$resp = $db->insertRow("asignacion",$asignacion);
		if ($resp) {
			return array('success'=>true,'msg'=>"asignacion registrada con exito");
		}else{
			return array('success'=>false,'msg'=>"error al registrar la asignacion");
		}
	}

	function editAsignacion($asignacion){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM asignacion where id=".$asignacion['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"La asignacion no se encuentra registrada");
		}else{
			$condition = array('id' =>  $asignacion['id']);
			$resp = $db->updateRows("asignacion",$asignacion,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"asignacion modificada con exito");
			}else{
				return array('success'=>false,'msg'=>"error al modificar la asignacion");
			}
		}
	}
 ?>