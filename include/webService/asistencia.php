<?php 
	$server->register("asistencia");
	$server->register("asistenciaEmpleado");
	$server->register("newAsistencia");
	$server->register("feriadoTrabajado");

	function asistencia($asistencia){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT a.*,nombre, apellido,b.cedula as cedula  FROM asistencia a right join empleado b on a.cedula=b.cedula where a.fecha='".$asistencia['fecha']."' or a.fecha is null");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"Asistencias registradas", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"No hay asistencias registradas");
		}
	}

	function asistenciaEmpleado($empleado){
		$db=new DB();


		$resp = $db->queryAll("SELECT * FROM asistencia where cedula='".$empleado['cedula']."' and fecha='".$empleado['fecha']."'");
		if ($resp==true) {
			return array('success'=>true,'msg'=>"Datos del empleado", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"El empleado aun no ha ingresado");
		}
	}
	
	function newAsistencia($asistencia){
		$db=new DB();

		if (isset($asistencia['hora_entrada'])) {
			$resp = $db->insertRow("asistencia",$asistencia);
		}else{
			$resp = $db-> queryRow("UPDATE asistencia set hora_salida='".$asistencia['hora_salida']."' where cedula='".$asistencia['cedula']."' and fecha='".$asistencia['fecha']."'");
		}
		 //se verifica si este dia es feriado
		$feriados = $db->queryAll("SELECT * FROM feriados");
		for ($i=0; $i < count($feriados) ; $i++) { 
			if ($asistencia['fecha'] == $feriados[$i]['fecha']) {
				$feriadosTrabajados=$db->queryRow("INSERT INTO dias_feriados (cedula, fecha, cant_dias) values ('".$asistencia['cedula']."','".$asistencia['fecha']."',1)");
			}
		}

		if ($resp==true) {
			return array('success'=>true,'msg'=>"Asistencias registrada", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"No hay asistencias registradas");
		}
	}
	
 ?>