<?php 
	include 'PDFClass.php';

	$server->register("nomina");
	$server->register("newNomina");
	$server->register("asistenciaNomina");
	$server->register("reciboNomina");
	$server->register("asistEmpleado");
	$server->register("asignacionEmpleado");
	$server->register("deduccionEmpleado");
	$server->register("tiempoTrabajado");
	$server->register("saveHExtra");


	function nomina($nomina=''){
		$db=new DB();
		
		$resp = $db->queryAll("SELECT b.letra, b.nombre, b.apellido, a.* FROM nomina a inner join empleado b on a.cedula=b.cedula");

		if ($resp==true ) {
			for ($i=0; $i < count($resp) ; $i++) { 

				if ($resp[$i]['fecha'] != null) {
					$day=explode("-", $resp[$i]['fecha']);

					if ($day[0] >= 15) {
						$fecha1=$day[0]."-".$day[1]."-01";
						$fecha2=$day[0]."-".$day[1]."-15";
					}else{
						$fecha1=$day[0]."-".$day[1]."-16";
						$fecha2=$day[0]."-".$day[1]."-31";
					}

					$dias_feriados=$db->queryAll("SELECT count(*) from dias_feriados where cedula ='".$resp[$i]['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

					$asignaciones=$db->queryAll("SELECT b.nombre, a.monto FROM asignacion_empleado a inner join asignacion b on b.id=a.asignacion where cedula='".$resp[$i]['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

					$deducciones=$db->queryAll("SELECT b.nombre, a.monto FROM deduccion_empleado a inner join deduccion b on b.id=a.deduccion where a.cedula='".$resp[$i]['cedula']."' and a.fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

					$dias_trabajados=$db->queryAll("SELECT count(*) as dias_trabajados from asistencia where cedula='".$resp[$i]['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

					$resp[$i]['feriados']=$dias_feriados[0]['count'];
					$resp[$i]['asignaciones']=$asignaciones;
					$resp[$i]['deducciones']=$deducciones;
					$resp[$i]['dias_trabajados']=$dias_trabajados[0]['dias_trabajados'];

				}
			}
			return array('success'=>true,'msg'=>"nómina registradas", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay nómina registradas");
	}

	function newNomina($nomina){
		$db=new DB();

		$empleado=$db->queryRow("SELECT * FROM empleado WHERE cedula='".$nomina['cedula']."'");

		if ($empleado) {

			

			$day=explode("-", $nomina['fecha']);

			if ($day[0] <= 15) {
				$fecha1=$day[0]."-".$day[1]."-01";
				$fecha2=$day[0]."-".$day[1]."-15";
				$cestaticket=0.0;
			}else{
				$fecha1=$day[0]."-".$day[1]."-16";
				$fecha2=$day[0]."-".$day[1]."-31";
				$cestaticket=$db->queryRow("SELECT cestaticket from configuracion");
				$cestaticket=$cestaticket['cestaticket'];

			}


			$asignacion= $db->queryRow("SELECT SUM(monto) from asignacion_empleado where cedula='".$nomina['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");
			$deduccion= $db->queryRow("SELECT SUM(monto) from deduccion_empleado where cedula='".$nomina['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");
			$montoFeriado=($empleado['sueldo']*1.5)+100;
			$dias_feriado=$db->queryAll("UPDATE dias_feriados SET monto=$montoFeriado where cedula ='".$nomina['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");
			$dias_feriado=$db->queryRow("SELECT SUM(monto) from dias_feriados where cedula='".$nomina['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");
				
			
			if ($asignacion['sum'] != null) {
				$asignacion=$asignacion['sum'];
			}else{
				$asignacion=0.0;
			}

			if ($deduccion['sum'] != null) {
				$deduccion=$deduccion['sum'];
			}else{
				$deduccion=0.0;
			}

			if ($dias_feriado['sum'] != null) {
				$dias_feriado=$dias_feriado['sum'];
			}else{
				$dias_feriado=0.0;
			}

			$mes=intval($day[1]);
			$anio=intval($day[2]);

			$asistencias=$db->queryRow("SELECT count(*) from asistencia where cedula='".$nomina['cedula']."' and fecha between '$fecha1' and '$fecha2'");
			if ($asistencias['count'] != null) {
				$asistencias=$asistencias['count'];
			}else{
				$asistencias=0;
			}
			
			$sueldo_neto=($empleado['sueldo']*$asistencias)-$deduccion;
			$sueldo_neto=$sueldo_neto+$dias_feriado+$asignacion;

			$existeNomina=$db->queryRow("SELECT * FROM nomina WHERE cedula='".$empleado['cedula']."' and fecha is null");

			if ($existeNomina) {
				$resp=$db->queryRow("UPDATE nomina set fecha='".$nomina['fecha']."', mes=$mes, anio=$anio, sueldo_diario=".$empleado['sueldo'].", total_asignaciones=$asignacion, total_deducciones=$deduccion, dias_feriados=$dias_feriado, sueldo_neto=$sueldo_neto, cestaticket=$cestaticket WHERE cedula='".$empleado['cedula']."' and fecha is null");

			}else{
				$resp=$db->queryRow("INSERT INTO nomina values ('".$nomina['fecha']."',$mes,$anio,'".$empleado['cedula']."',".$empleado['sueldo'].",$asignacion,$deduccion,0,$dias_feriado,$sueldo_neto,$cestaticket,0)");
				
			}


			if (!$resp) {
				return array('success'=>true,'msg'=>"empleado registrado en nómina", 'data'=>$resp);
			}else{
				return array('success'=>false,'msg'=>"Error en el registro", 'data'=>$resp);
			}
		}
	}

	function asistenciaNomina($asistencia){
		$db=new DB();
		$where = ($_SESSION['tipo']=='admin')?" and b.sucursal=".$_SESSION['sucursal']:'';
		$resp = $db->queryAll("SELECT a.cedula, b.nombre, b.apellido, b.cargo from asistencia a inner join empleado b on a.cedula=b.cedula where a.fecha BETWEEN  '".$asistencia['fecha1']."' and '".$asistencia['fecha2']."' $where group by a.cedula, b.nombre, b.apellido, b.cargo");

		if ($resp==true) {
			return array('success'=>true,'msg'=>"empleados que entran en esta nómina", 'data'=>$resp);
		}
		return array('success'=>false,'msg'=>"No hay empleados en esta nómina");
	}

	function reciboNomina($nomina){
		$db=new DB();
		
		$resp = $db->queryRow("SELECT b.letra, b.nombre, b.apellido, b.fecha_ingreso, a.* FROM nomina a inner join empleado b on a.cedula=b.cedula where a.cedula='".$nomina['cedula']."'");

		if ($resp==true) {

			$day=explode("-", $resp['fecha']);

			if ($day[0] >= 15) {
				$fecha1=$day[0]."-".$day[1]."-01";
				$fecha2=$day[0]."-".$day[1]."-15";
			}else{
				$fecha1=$day[0]."-".$day[1]."-16";
				$fecha2=$day[0]."-".$day[1]."-31";
			}

			$dias_feriados=$db->queryAll("SELECT count(*) from dias_feriados where cedula ='".$resp['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

			$asignaciones=$db->queryAll("SELECT b.nombre, a.monto FROM asignacion_empleado a inner join asignacion b on b.id=a.asignacion where cedula='".$resp['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

			$deducciones=$db->queryAll("SELECT b.nombre, a.monto FROM deduccion_empleado a inner join deduccion b on b.id=a.deduccion where a.cedula='".$resp['cedula']."' and a.fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

			$dias_trabajados=$db->queryAll("SELECT count(*) as dias_trabajados from asistencia where cedula='".$resp['cedula']."' and fecha BETWEEN '".$fecha1."' and '".$fecha2."'");

				$resp['feriados']=$dias_feriados[0]['count'];
				$resp['asignaciones']=$asignaciones;
				$resp['deducciones']=$deducciones;
				$resp['dias_trabajados']=$dias_trabajados[0]['dias_trabajados'];
			}

			$sueldo_mensual=$resp['sueldo_diario']*30;

			$asig='';
			if ($resp['asignaciones']) {
				for ($i=0; $i < count($resp['asignaciones']); $i++) { 
					$asig .='<tr>
						<td>'.$resp['asignaciones'][$i]['nombre'].' (Asignación)</td>
						<td>1</td>
						<td>'.$resp['asignaciones'][$i]['monto'].'</td>
						<td>'.$resp['asignaciones'][$i]['monto'].'</td>
					</tr>';
				}
			}

			$deduc='';
			if ($resp['deducciones']) {
				for ($i=0; $i < count($resp['deducciones']); $i++) { 
					$deduc .='<tr>
						<td>'.$resp['deducciones'][$i]['nombre'].' (Deducción)</td>
						<td>1</td>
						<td>'.$resp['deducciones'][$i]['monto'].'</td>
						<td>'.$resp['deducciones'][$i]['monto'].'</td>
					</tr>';
				}
			}



			$data='';
			$data .= '<tr>
						<td>Quincena</td>
						<td>'.$resp['dias_trabajados'].'</td>
						<td>'.$resp['sueldo_diario'].'</td>
						<td>'.$resp['sueldo_diario'] * $resp['dias_trabajados'].'</td>
					</tr>
					<tr>
						<td>Feriados</td>
						<td>'.$resp['feriados'].'</td>
						<td>'.$resp['dias_feriados'] / $resp['feriados'].'</td>
						<td>'.$resp['dias_feriados'].'</td>
					</tr>'
					.$asig.''
					.$deduc.'
					<tr>
						<td>Sueldo Neto</td>
						<td></td>
						<td></td>
						<td>'.$resp['sueldo_neto'].'</td>
					</tr>';

			$content='
				<div>
					<p>Empleado:'.$resp['nombre'].' '.$resp['apellido'].'</p>
					<p>Cédula: '.$resp['cedula'].'</p>
					<p>Fecha de ingreso: '.$resp['fecha_ingreso'].'</p>
				</div>
				<table class="table row-border table-bordered">
					<thead>
			            <tr>
			                <th>Descripción</th>
			                <th>Días / Cant</th>
			                <th>Monto (BsF)</th>
			                <th>Total (BsF)</th>
			            </tr>
			        </thead>
			        <tbody>
						'.$data.'
			        </tbody>

				</table>

				Firmo conforme: _______________________';
			

			
			$name='Recibo_'.date("Ymdgis").'.pdf';
			newPdf($content,'Recibo de pago','pdf/'.$name,$resp['cedula']);
			return array('data' => $name,'success'=>true,'num_factura'=>$resp['cedula']);

			
	}

	function asistEmpleado($empleado){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM asistencia where cedula='".$empleado['cedula']."' and fecha  BETWEEN '".$empleado['fecha1']."' and '".$empleado['fecha2']."'");
		if ($resp==true) {
			return array('success'=>true,'msg'=>"Datos del empleado", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"El empleado aun no ha ingresado");
		}
	}

	function asignacionEmpleado($empleado_asignacion){
		$db=new DB();
		$asignaciones=$empleado_asignacion['asignaciones'];
		for ($i=0; $i < count($asignaciones); $i++) { 
			if ($asignaciones[$i]['monto'] != null) {
				$resp= $db->queryRow("INSERT INTO asignacion_empleado (cedula, asignacion, monto) values ('".$empleado_asignacion['cedula']."',".$asignaciones[$i]['asignacion'].", ".$asignaciones[$i]['monto'].")");
			}else{
				$empleado=$db->queryRow("SELECT sueldo from empleado where cedula='".$empleado_asignacion['cedula']."'");
				$monto=($empleado['sueldo']*30)*($asignaciones[$i]['porcentaje']/100);
				$resp= $db->queryRow("INSERT INTO asignacion_empleado (cedula, asignacion, monto) values ('".$empleado_asignacion['cedula']."',".$asignaciones[$i]['asignacion'].", $monto)");
			}
		}

		if (!$resp) {
			return array('success'=>true,'msg'=>"Asignación agregada con exito",'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"error al agregar asignación");
		}
	}

	function deduccionEmpleado($empleado_deduccion){
		$db=new DB();
		$deducciones=$empleado_deduccion['deducciones'];
		for ($i=0; $i < count($deducciones); $i++) { 
			if ($deducciones[$i]['monto'] != null) {
				$resp= $db->queryRow("INSERT INTO deduccion_empleado (cedula, deduccion, monto) values ('".$empleado_deduccion['cedula']."',".$deducciones[$i]['asignacion'].", ".$deducciones[$i]['monto'].")");
			}else{
				$empleado=$db->queryRow("SELECT sueldo from empleado where cedula='".$empleado_asignacion['cedula']."'");
				$monto=($empleado['sueldo']*30)*($deducciones[$i]['porcentaje']/100);
				$resp= $db->queryRow("INSERT INTO asignacion_empleado (cedula, asignacion, monto) values ('".$empleado_deduccion['cedula']."',".$deducciones[$i]['asignacion'].", $monto)");
			}
		}

		if (!$resp) {
			return array('success'=>true,'msg'=>"Deducción agregada con exito",'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"error al agregar deducción");
		}
	}

	function tiempoTrabajado($nomina){
		$db = new DB();
		$data=$db->queryAll("SELECT hora_entrada, hora_salida, fecha from asistencia where cedula='".$nomina['cedula']."' and fecha BETWEEN '".$nomina['fecha1']."' and '".$nomina['fecha2']."'");

		$jornadaTrabajo=$db->queryAll("SELECT * FROM horario");
		$resp['horario']=$jornadaTrabajo[0];
		$resp['asistencia']=$data;

		if ($resp) {
			return array('success'=>true,'msg'=>"Asistencia del empleado",'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"error no hay asistencia", 'data'=>$resp);
		}
	}

	function saveHExtra($horas){
		$db = new DB();
		$day=explode("-", $horas['fecha']);
		$mes=intval($day[1]);
		$anio=intval($day[0]);

		$empleado=$db->queryRow("SELECT * FROM empleado WHERE cedula='".$horas['cedula']."'");
		$jornada=$db->queryRow("SELECT jornada from horario");

		$horaExtra=($empleado['sueldo']/$jornada['jornada'])*1.5;
		$horaExtra=$horas['hora']*$horaExtra;



		$resp=$db->queryRow("INSERT into nomina (mes, anio, cedula, horas_extras, total_hextras) values ($mes,$anio,'".$horas['cedula']."',".$horaExtra.",".$horas['hora'].")");

		if (!$resp) {
			return array('success'=>true,'msg'=>"Hora extra registrada en nómina", 'data'=>$resp);
		}else{
			return array('success'=>false,'msg'=>"Error en el registro", 'data'=>$resp);
		}

	}


 ?>