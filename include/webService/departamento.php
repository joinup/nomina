<?php 
	$server->register("departamentos");
	$server->register("newDepartamentos");
	$server->register("editDepartamentos");
	$server->register("asignarDepartamentoSucursal");
	$server->register("asignarSucursalDepartamento");

	function departamentos($departamento){
		$db=new DB();
		$where = ($_SESSION['tipo']=='admin')?" and b.sucursal=".$_SESSION['sucursal']:'';
		$resp = $db->queryAll("SELECT a.* FROM departamento a left join sucursal_departamento b on a.id=b.sucursal where a.estado='".$departamento['estado']."'".$where);
		if ($resp==true) {
			$sucursal_depa=array();
			for ($i=0; $i < count($resp); $i++) { 
				$sucursal_depa[$i]['id']=$resp[$i]['id'];
				$sucursal_depa[$i]['nombre']=$resp[$i]['nombre'];
				$sucursal_depa[$i]['estado']=$resp[$i]['estado'];
				$sucursales = $db->queryAll("SELECT a.id,a.nombre from sucursal a left join sucursal_departamento b on a.id=b.sucursal where b.departamento=".$resp[$i]['id']);
				$sucursal_depa[$i]['sucursales']=$sucursales;
			}
			return array('success'=>true,'msg'=>"departamentos registrados", 'data'=>$sucursal_depa);
		}
		return array('success'=>false,'msg'=>"No hay departamentos registrados");
	}

	function newDepartamentos($departamento){
		$db=new DB();
		
		$resp = $db->queryRow("INSERT into departamento (nombre) values ('".$departamento['nombre']."') RETURNING id");
		if ($resp) {
			return array('success'=>true,'msg'=>'Departamento registrado con exito');
		}else{
			return array('success'=>false,'msg'=>"error al registrar el departamento");
		}
	}
	function asignarDepartamentoSucursal($data){
		$db=new DB();
		
		$db->begin();
		foreach ($data['departamento'] as $value) {
			//$insert = array('departamento'=>$value,'sucursal'=>$data['sucursal']);
			$sucursal=$data['sucursal'];
			$resp = $db->queryRow("INSERT INTO sucursal_departamento VALUES ($sucursal,$value)");
			if ($resp) {
				$db->rollback();
				return array('success'=>false,'msg'=>"error al registrar el departamento");
			}
		}
		$db->finish();
		return array('success'=>true,'msg'=>"departamento registrado con exito");
	}
	function asignarSucursalDepartamento($data,$id){
		$db=new DB();
		
		$db->begin();
		foreach ($data['sucursal'] as $value) {
			//$insert = array('departamento'=>$id,'sucursal'=>$value);
			$depa=$id['id'];
			$resp = $db->queryRow("INSERT INTO sucursal_departamento VALUES ($value,$depa)");
			if ($resp) {
				$db->rollback();
				return array('success'=>false,'msg'=>"error al registrar el departamento");
			}
		}
		$db->finish();
		return array('success'=>true,'msg'=>"departamento registrado con exito");
	}

	function editDepartamentos($departamento){
		$db=new DB();

		$resp = $db->queryAll("SELECT * FROM departamento where id=".$departamento['id']);
		if (!$resp) {
			return array('success'=>false,'msg'=>"el departamento no se encuentra registrado");
		}else{
			$condition = array('id' =>  $departamento['id']);
			$resp = $db->updateRows("departamento",$departamento,$condition);
			if ($resp) {
				return array('success'=>true,'msg'=>"departamento modificado con exito");
			}else{
				return array('success'=>false,'msg'=>"error a modificar el departamento");
			}
		}
	}
 ?>