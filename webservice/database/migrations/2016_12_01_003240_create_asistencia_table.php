<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsistenciaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asistencia', function(Blueprint $table)
		{
			$table->string('cedula', 12);
			$table->date('fecha')->default('now');
			$table->time('hora_entrada')->nullable()->default('now');
			$table->time('hora_salida')->nullable();
			$table->primary(['cedula','fecha'], 'pk_asistencia');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asistencia');
	}

}
