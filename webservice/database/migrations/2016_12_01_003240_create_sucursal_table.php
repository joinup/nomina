<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSucursalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sucursal', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 80);
			$table->string('direccion')->nullable();
			$table->string('telefono', 18)->nullable();
			$table->string('estado', 10)->nullable()->default('activo');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sucursal');
	}

}
