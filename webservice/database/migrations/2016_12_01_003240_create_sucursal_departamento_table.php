<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSucursalDepartamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sucursal_departamento', function(Blueprint $table)
		{
			$table->integer('sucursal');
			$table->integer('departamento');
			$table->primary(['sucursal','departamento'], 'pk_sucursal_departamento');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sucursal_departamento');
	}

}
