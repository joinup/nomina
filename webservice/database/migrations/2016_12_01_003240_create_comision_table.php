<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComisionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comision', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('cedula', 12);
			$table->date('fecha')->nullable();
			$table->decimal('monto', 16)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comision');
	}

}
