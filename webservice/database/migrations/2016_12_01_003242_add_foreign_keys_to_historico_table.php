<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHistoricoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('historico', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_historico_empleado')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cargo', 'fk_historico_cargo')->references('id')->on('cargo')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('historico', function(Blueprint $table)
		{
			$table->dropForeign('fk_historico_empleado');
			$table->dropForeign('fk_historico_cargo');
		});
	}

}
