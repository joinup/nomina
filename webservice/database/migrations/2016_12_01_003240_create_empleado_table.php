<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empleado', function(Blueprint $table)
		{
			$table->string('cedula', 12)->primary('pk_empleado');
			$table->string('nombre', 40);
			$table->string('apellido', 40);
			$table->string('direccion', 100)->nullable();
			$table->string('telefono', 18)->nullable();
			$table->string('discapacidad', 100)->nullable();
			$table->char('sexo', 1)->nullable();
			$table->date('fec_nac')->nullable();
			$table->string('email', 60)->nullable();
			$table->integer('cargo')->nullable();
			$table->decimal('sueldo', 16)->nullable();
			$table->string('estado', 10)->nullable()->default('activo');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empleado');
	}

}
