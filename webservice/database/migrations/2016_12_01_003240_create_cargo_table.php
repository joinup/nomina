<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCargoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cargo', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 80);
			$table->string('descripcion', 200)->nullable();
			$table->string('estado', 10)->nullable()->default('activo');
			$table->boolean('comision')->nullable()->default(false);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cargo');
	}

}
