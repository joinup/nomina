<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNominaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nomina', function(Blueprint $table)
		{
			$table->date('fecha')->nullable();
			$table->integer('mes');
			$table->integer('anio');
			$table->string('cedula', 12);
			$table->decimal('sueldo_diario', 16)->nullable();
			$table->decimal('total_asignaciones', 16)->nullable();
			$table->decimal('total_deducciones', 16)->nullable();
			$table->decimal('horas_extras', 16)->nullable();
			$table->decimal('comision', 16)->nullable();
			$table->decimal('dias_feriados', 16)->nullable();
			$table->decimal('sueldo_neto', 16)->nullable();
			$table->primary(['mes','anio','cedula'], 'pk_nomina');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nomina');
	}

}
