<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHoraExtraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hora_extra', function(Blueprint $table)
		{
			$table->string('cedula', 12);
			$table->date('fecha');
			$table->integer('cantidad')->nullable();
			$table->decimal('monto', 16)->nullable();
			$table->primary(['cedula','fecha'], 'pk_hora_extra');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hora_extra');
	}

}
