<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoricoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('historico', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('cedula', 12);
			$table->date('fecha_inicio')->nullable();
			$table->date('fecha_final')->nullable();
			$table->integer('cargo')->nullable();
			$table->decimal('sueldo', 16)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('historico');
	}

}
