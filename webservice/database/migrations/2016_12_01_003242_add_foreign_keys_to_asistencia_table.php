<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAsistenciaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asistencia', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_empleado_asistencia')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asistencia', function(Blueprint $table)
		{
			$table->dropForeign('fk_empleado_asistencia');
		});
	}

}
