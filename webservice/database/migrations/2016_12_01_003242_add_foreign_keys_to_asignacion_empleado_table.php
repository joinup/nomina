<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAsignacionEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asignacion_empleado', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_empleado_asignacion_empleado')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('asignacion', 'fk_asignacion_asignacion_empleado')->references('id')->on('asignacion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asignacion_empleado', function(Blueprint $table)
		{
			$table->dropForeign('fk_empleado_asignacion_empleado');
			$table->dropForeign('fk_asignacion_asignacion_empleado');
		});
	}

}
