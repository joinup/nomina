<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHoraExtraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hora_extra', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_empleado_hora_extra')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hora_extra', function(Blueprint $table)
		{
			$table->dropForeign('fk_empleado_hora_extra');
		});
	}

}
