<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empleado', function(Blueprint $table)
		{
			$table->foreign('cargo', 'pk_empleado_cargo')->references('id')->on('cargo')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empleado', function(Blueprint $table)
		{
			$table->dropForeign('pk_empleado_cargo');
		});
	}

}
