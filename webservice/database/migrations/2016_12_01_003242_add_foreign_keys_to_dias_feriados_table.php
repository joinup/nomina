<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDiasFeriadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dias_feriados', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_empleado_dias_feriados')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dias_feriados', function(Blueprint $table)
		{
			$table->dropForeign('fk_empleado_dias_feriados');
		});
	}

}
