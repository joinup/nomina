<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeduccionEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('deduccion_empleado', function(Blueprint $table)
		{
			$table->foreign('cedula', 'fk_empleado_deduccion_empleado')->references('cedula')->on('empleado')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('deduccion', 'fk_deduccion_deduccion_empleado')->references('id')->on('deduccion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('deduccion_empleado', function(Blueprint $table)
		{
			$table->dropForeign('fk_empleado_deduccion_empleado');
			$table->dropForeign('fk_deduccion_deduccion_empleado');
		});
	}

}
