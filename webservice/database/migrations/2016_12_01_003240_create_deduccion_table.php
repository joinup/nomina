<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeduccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deduccion', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 40);
			$table->decimal('monto', 16)->nullable();
			$table->decimal('porcentaje', 16)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deduccion');
	}

}
