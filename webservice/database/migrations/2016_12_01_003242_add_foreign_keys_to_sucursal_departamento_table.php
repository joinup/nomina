<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSucursalDepartamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sucursal_departamento', function(Blueprint $table)
		{
			$table->foreign('sucursal', 'fk_sucursal_sucursal_departamento')->references('id')->on('sucursal')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('departamento', 'fk_departamento_sucursal_departamento')->references('id')->on('departamento')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sucursal_departamento', function(Blueprint $table)
		{
			$table->dropForeign('fk_sucursal_sucursal_departamento');
			$table->dropForeign('fk_departamento_sucursal_departamento');
		});
	}

}
