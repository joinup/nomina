<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsignacionEmpleadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asignacion_empleado', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('cedula', 12);
			$table->integer('asignacion');
			$table->decimal('monto', 16)->nullable();
			$table->date('fecha')->nullable();
			$table->string('tipo', 30)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asignacion_empleado');
	}

}
